#ifndef EZTRACE_HTABLE_H
#define EZTRACE_HTABLE_H

#include <stdint.h>
#include "eztrace_spinlock.h"

typedef uint32_t hashkey_t;

struct ezt_hashtable_entry {
  hashkey_t key;
  void* data;
  struct ezt_hashtable_entry* next;
};

struct ezt_hashtable_list {
  struct ezt_hashtable_entry *entries;
  ezt_spinlock lock;
};

struct ezt_hashtable {
  int table_len;
  struct ezt_hashtable_list* table;
};

/* return a hash of str_key */
hashkey_t hash_function_str(char *str_key);
hashkey_t hash_function_int32(uint32_t int_key);
hashkey_t hash_function_int64(uint64_t int_key);
hashkey_t hash_function_ptr(void* ptr_key);

/* initialize a hashtable */
void ezt_hashtable_init(struct ezt_hashtable*table,
			int table_len);
/* delete a hashtable */
void ezt_hashtable_finalize(struct ezt_hashtable*table);

/* return the data associated with key */
void* ezt_hashtable_get(struct ezt_hashtable*table,
			hashkey_t key);

/* insert data in the hashtable. If key is already in the hashtable, its data is overwritten*/
void ezt_hashtable_insert(struct ezt_hashtable*table,
			  hashkey_t key,
			  void* data);

/* remove an entry from the hashtable */
void ezt_hashtable_remove(struct ezt_hashtable*table,
			  hashkey_t key);
		       
void ezt_hashtable_print(struct ezt_hashtable *table);

#endif	/* EZTRACE_HTABLE_H */
