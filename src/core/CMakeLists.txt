configure_file(${CMAKE_CURRENT_SOURCE_DIR}/eztrace_config.h.in ${CMAKE_CURRENT_BINARY_DIR}/include/eztrace-core/eztrace_config.h)

add_library(eztrace-core SHARED
  ezt_demangle.c
  eztrace_array.c
  eztrace_htable.c
  eztrace_sampling.c
  eztrace_config.c
  include/eztrace-core/ezt_demangle.h
  include/eztrace-core/eztrace_array.h
  include/eztrace-core/eztrace_htable.h
  include/eztrace-core/eztrace_list.h
  include/eztrace-core/eztrace_macros.h
  include/eztrace-core/eztrace_sampling.h
  include/eztrace-core/eztrace_spinlock.h
  include/eztrace-core/eztrace_stack.h
  include/eztrace-core/eztrace_types.h
  include/eztrace-core/eztrace_attributes.h
  include/eztrace-core/types.h
  include/eztrace-core/eztrace_utils.h
)

target_include_directories(eztrace-core
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDE_DIR}>
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
)

target_compile_options(eztrace-core
  PRIVATE
    -Wall -Wextra
    -Werror
)

if (HAVE_DEMANGLE)
  target_include_directories(eztrace-core
    PRIVATE
      ${DEMANGLE_INCLUDEDIR}
  )
  target_compile_options(eztrace-core
    PRIVATE
      -DHAVE_DEMANGLE
  )
  target_link_libraries(eztrace-core
    PRIVATE
      iberty
  )
endif()

install(TARGETS eztrace-core EXPORT EZTraceCoreTargets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/eztrace-core DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/include/eztrace-core DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
