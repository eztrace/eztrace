/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <eztrace-core/eztrace_config.h>
#include <eztrace-instrumentation/pptrace.h>
#include <eztrace-lib/eztrace.h>
#include <eztrace-lib/eztrace_module.h>

#include "mpi.h"
#include "netcdf.h"

/* set to 1 when all the hooks are set.
 * This is usefull in order to avoid recursive calls to mutex_lock for example
 */
static volatile int _netcdf_initialized = 0;

#define CURRENT_MODULE netcdf
DECLARE_CURRENT_MODULE;

/* pointers to actual netcdf functions */


int (*libnc_def_user_format)(int mode_flag, NC_Dispatch * dispatch_table, char *magic_number) = NULL;
int (*libnc_inq_user_format)(int mode_flag, NC_Dispatch ** dispatch_table, char *magic_number) = NULL;
int (*libnc_set_alignment)(int threshold, int alignment) = NULL;
int (*libnc_get_alignment)(int *thresholdp, int *alignmentp) = NULL;
int (*libnc__create)(const char *path, int cmode, size_t initialsz, size_t *chunksizehintp, int *ncidp) = NULL;
int (*libnc_create)(const char *path, int cmode, int *ncidp) = NULL;
int (*libnc__open)(const char *path, int mode, size_t *chunksizehintp, int *ncidp) = NULL;
int (*libnc_open)(const char *path, int mode, int *ncidp) = NULL;
int (*libnc_inq_path)(int ncid, size_t *pathlen, char *path) = NULL;
int (*libnc_inq_ncid)(int ncid, const char *name, int *grp_ncid) = NULL;
int (*libnc_inq_grps)(int ncid, int *numgrps, int *ncids) = NULL;
int (*libnc_inq_grpname)(int ncid, char *name) = NULL;
int (*libnc_inq_grpname_full)(int ncid, size_t *lenp, char *full_name) = NULL;
int (*libnc_inq_grpname_len)(int ncid, size_t *lenp) = NULL;
int (*libnc_inq_grp_parent)(int ncid, int *parent_ncid) = NULL;
int (*libnc_inq_grp_ncid)(int ncid, const char *grp_name, int *grp_ncid) = NULL;
int (*libnc_inq_grp_full_ncid)(int ncid, const char *full_name, int *grp_ncid) = NULL;
int (*libnc_inq_varids)(int ncid, int *nvars, int *varids) = NULL;
int (*libnc_inq_dimids)(int ncid, int *ndims, int *dimids, int include_parents) = NULL;
int (*libnc_inq_typeids)(int ncid, int *ntypes, int *typeids) = NULL;
int (*libnc_inq_type_equal)(int ncid1, nc_type typeid1, int ncid2, nc_type typeid2, int *equal) = NULL;
int (*libnc_def_grp)(int parent_ncid, const char *name, int *new_ncid) = NULL;
int (*libnc_rename_grp)(int grpid, const char *name) = NULL;
int (*libnc_def_compound)(int ncid, size_t size, const char *name, nc_type * typeidp) = NULL;
int (*libnc_insert_compound)(int ncid, nc_type xtype, const char *name, size_t offset, nc_type field_typeid) = NULL;
int (*libnc_insert_array_compound)(int ncid, nc_type xtype, const char *name, size_t offset, nc_type field_typeid, int ndims, const int *dim_sizes) = NULL;
int (*libnc_inq_type)(int ncid, nc_type xtype, char *name, size_t *size) = NULL;
int (*libnc_inq_typeid)(int ncid, const char *name, nc_type * typeidp) = NULL;
int (*libnc_inq_compound)(int ncid, nc_type xtype, char *name, size_t *sizep, size_t *nfieldsp) = NULL;
int (*libnc_inq_compound_name)(int ncid, nc_type xtype, char *name) = NULL;
int (*libnc_inq_compound_size)(int ncid, nc_type xtype, size_t *sizep) = NULL;
int (*libnc_inq_compound_nfields)(int ncid, nc_type xtype, size_t *nfieldsp) = NULL;
int (*libnc_inq_compound_field)(int ncid, nc_type xtype, int fieldid, char *name, size_t *offsetp, nc_type * field_typeidp, int *ndimsp, int *dim_sizesp) = NULL;
int (*libnc_inq_compound_fieldname)(int ncid, nc_type xtype, int fieldid, char *name) = NULL;
int (*libnc_inq_compound_fieldindex)(int ncid, nc_type xtype, const char *name, int *fieldidp) = NULL;
int (*libnc_inq_compound_fieldoffset)(int ncid, nc_type xtype, int fieldid, size_t *offsetp) = NULL;
int (*libnc_inq_compound_fieldtype)(int ncid, nc_type xtype, int fieldid, nc_type * field_typeidp) = NULL;
int (*libnc_inq_compound_fieldndims)(int ncid, nc_type xtype, int fieldid, int *ndimsp) = NULL;
int (*libnc_inq_compound_fielddim_sizes)(int ncid, nc_type xtype, int fieldid, int *dim_sizes) = NULL;
int (*libnc_def_vlen)(int ncid, const char *name, nc_type base_typeid, nc_type * xtypep) = NULL;
int (*libnc_inq_vlen)(int ncid, nc_type xtype, char *name, size_t *datum_sizep, nc_type * base_nc_typep) = NULL;
int (*libnc_free_vlen)(nc_vlen_t * vl) = NULL;
int (*libnc_free_vlens)(size_t len, nc_vlen_t vlens[]) = NULL;
int (*libnc_put_vlen_element)(int ncid, int typeid1, void *vlen_element, size_t len, const void *data) = NULL;
int (*libnc_get_vlen_element)(int ncid, int typeid1, const void *vlen_element, size_t *len, void *data) = NULL;
int (*libnc_free_string)(size_t len, char **data) = NULL;
int (*libnc_inq_user_type)(int ncid, nc_type xtype, char *name, size_t *size, nc_type * base_nc_typep, size_t *nfieldsp, int *classp) = NULL;
int (*libnc_put_att)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const void *op) = NULL;
int (*libnc_get_att)(int ncid, int varid, const char *name, void *ip) = NULL;
int (*libnc_def_enum)(int ncid, nc_type base_typeid, const char *name, nc_type * typeidp) = NULL;
int (*libnc_insert_enum)(int ncid, nc_type xtype, const char *name, const void *value) = NULL;
int (*libnc_inq_enum)(int ncid, nc_type xtype, char *name, nc_type * base_nc_typep, size_t *base_sizep, size_t *num_membersp) = NULL;
int (*libnc_inq_enum_member)(int ncid, nc_type xtype, int idx, char *name, void *value) = NULL;
int (*libnc_inq_enum_ident)(int ncid, nc_type xtype, long long value, char *identifier) = NULL;
int (*libnc_def_opaque)(int ncid, size_t size, const char *name, nc_type * xtypep) = NULL;
int (*libnc_inq_opaque)(int ncid, nc_type xtype, char *name, size_t *sizep) = NULL;
int (*libnc_put_var)(int ncid, int varid, const void *op) = NULL;
int (*libnc_get_var)(int ncid, int varid, void *ip) = NULL;
int (*libnc_put_var1)(int ncid, int varid, const size_t *indexp, const void *op) = NULL;
int (*libnc_get_var1)(int ncid, int varid, const size_t *indexp, void *ip) = NULL;
int (*libnc_put_vara)(int ncid, int varid, const size_t *startp, const size_t *countp, const void *op) = NULL;
int (*libnc_get_vara)(int ncid, int varid, const size_t *startp, const size_t *countp, void *ip) = NULL;
int (*libnc_put_vars)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const void *op) = NULL;
int (*libnc_get_vars)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, void *ip) = NULL;
int (*libnc_put_varm)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const void *op) = NULL;
int (*libnc_get_varm)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, void *ip) = NULL;
int (*libnc_def_var_quantize)(int ncid, int varid, int quantize_mode, int nsd) = NULL;
int (*libnc_inq_var_quantize)(int ncid, int varid, int *quantize_modep, int *nsdp) = NULL;
int (*libnc_def_var_deflate)(int ncid, int varid, int shuffle, int deflate, int deflate_level) = NULL;
int (*libnc_inq_var_deflate)(int ncid, int varid, int *shufflep, int *deflatep, int *deflate_levelp) = NULL;
int (*libnc_def_var_szip)(int ncid, int varid, int options_mask, int pixels_per_block) = NULL;
int (*libnc_inq_var_szip)(int ncid, int varid, int *options_maskp, int *pixels_per_blockp) = NULL;
int (*libnc_def_var_fletcher32)(int ncid, int varid, int fletcher32) = NULL;
int (*libnc_inq_var_fletcher32)(int ncid, int varid, int *fletcher32p) = NULL;
int (*libnc_def_var_chunking)(int ncid, int varid, int storage, const size_t *chunksizesp) = NULL;
int (*libnc_inq_var_chunking)(int ncid, int varid, int *storagep, size_t *chunksizesp) = NULL;
int (*libnc_def_var_fill)(int ncid, int varid, int no_fill, const void *fill_value) = NULL;
int (*libnc_inq_var_fill)(int ncid, int varid, int *no_fill, void *fill_valuep) = NULL;
int (*libnc_def_var_endian)(int ncid, int varid, int endian) = NULL;
int (*libnc_inq_var_endian)(int ncid, int varid, int *endianp) = NULL;
int (*libnc_def_var_filter)(int ncid, int varid, unsigned int id, size_t nparams, const unsigned int *parms) = NULL;
int (*libnc_inq_var_filter)(int ncid, int varid, unsigned int *idp, size_t *nparams, unsigned int *params) = NULL;
int (*libnc_set_fill)(int ncid, int fillmode, int *old_modep) = NULL;
int (*libnc_set_default_format)(int format, int *old_formatp) = NULL;
int (*libnc_set_chunk_cache)(size_t size, size_t nelems, float preemption) = NULL;
int (*libnc_get_chunk_cache)(size_t *sizep, size_t *nelemsp, float *preemptionp) = NULL;
int (*libnc_set_var_chunk_cache)(int ncid, int varid, size_t size, size_t nelems, float preemption) = NULL;
int (*libnc_get_var_chunk_cache)(int ncid, int varid, size_t *sizep, size_t *nelemsp, float *preemptionp) = NULL;
int (*libnc_redef)(int ncid) = NULL;
int (*libnc__enddef)(int ncid, size_t h_minfree, size_t v_align, size_t v_minfree, size_t r_align) = NULL;
int (*libnc_enddef)(int ncid) = NULL;
int (*libnc_sync)(int ncid) = NULL;
int (*libnc_abort)(int ncid) = NULL;
int (*libnc_close)(int ncid) = NULL;
int (*libnc_inq)(int ncid, int *ndimsp, int *nvarsp, int *nattsp, int *unlimdimidp) = NULL;
int (*libnc_inq_ndims)(int ncid, int *ndimsp) = NULL;
int (*libnc_inq_nvars)(int ncid, int *nvarsp) = NULL;
int (*libnc_inq_natts)(int ncid, int *nattsp) = NULL;
int (*libnc_inq_unlimdim)(int ncid, int *unlimdimidp) = NULL;
int (*libnc_inq_unlimdims)(int ncid, int *nunlimdimsp, int *unlimdimidsp) = NULL;
int (*libnc_inq_format)(int ncid, int *formatp) = NULL;
int (*libnc_inq_format_extended)(int ncid, int *formatp, int *modep) = NULL;
int (*libnc_def_dim)(int ncid, const char *name, size_t len, int *idp) = NULL;
int (*libnc_inq_dimid)(int ncid, const char *name, int *idp) = NULL;
int (*libnc_inq_dim)(int ncid, int dimid, char *name, size_t *lenp) = NULL;
int (*libnc_inq_dimname)(int ncid, int dimid, char *name) = NULL;
int (*libnc_inq_dimlen)(int ncid, int dimid, size_t *lenp) = NULL;
int (*libnc_rename_dim)(int ncid, int dimid, const char *name) = NULL;
int (*libnc_inq_att)(int ncid, int varid, const char *name, nc_type * xtypep, size_t *lenp) = NULL;
int (*libnc_inq_attid)(int ncid, int varid, const char *name, int *idp) = NULL;
int (*libnc_inq_atttype)(int ncid, int varid, const char *name, nc_type * xtypep) = NULL;
int (*libnc_inq_attlen)(int ncid, int varid, const char *name, size_t *lenp) = NULL;
int (*libnc_inq_attname)(int ncid, int varid, int attnum, char *name) = NULL;
int (*libnc_copy_att)(int ncid_in, int varid_in, const char *name, int ncid_out, int varid_out) = NULL;
int (*libnc_rename_att)(int ncid, int varid, const char *name, const char *newname) = NULL;
int (*libnc_del_att)(int ncid, int varid, const char *name) = NULL;
int (*libnc_put_att_text)(int ncid, int varid, const char *name, size_t len, const char *op) = NULL;
int (*libnc_get_att_text)(int ncid, int varid, const char *name, char *ip) = NULL;
int (*libnc_put_att_string)(int ncid, int varid, const char *name, size_t len, const char **op) = NULL;
int (*libnc_get_att_string)(int ncid, int varid, const char *name, char **ip) = NULL;
int (*libnc_put_att_uchar)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned char *op) = NULL;
int (*libnc_get_att_uchar)(int ncid, int varid, const char *name, unsigned char *ip) = NULL;
int (*libnc_put_att_schar)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const signed char *op) = NULL;
int (*libnc_get_att_schar)(int ncid, int varid, const char *name, signed char *ip) = NULL;
int (*libnc_put_att_short)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const short *op) = NULL;
int (*libnc_get_att_short)(int ncid, int varid, const char *name, short *ip) = NULL;
int (*libnc_put_att_int)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const int *op) = NULL;
int (*libnc_get_att_int)(int ncid, int varid, const char *name, int *ip) = NULL;
int (*libnc_put_att_long)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const long *op) = NULL;
int (*libnc_get_att_long)(int ncid, int varid, const char *name, long *ip) = NULL;
int (*libnc_put_att_float)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const float *op) = NULL;
int (*libnc_get_att_float)(int ncid, int varid, const char *name, float *ip) = NULL;
int (*libnc_put_att_double)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const double *op) = NULL;
int (*libnc_get_att_double)(int ncid, int varid, const char *name, double *ip) = NULL;
int (*libnc_put_att_ushort)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned short *op) = NULL;
int (*libnc_get_att_ushort)(int ncid, int varid, const char *name, unsigned short *ip) = NULL;
int (*libnc_put_att_uint)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned int *op) = NULL;
int (*libnc_get_att_uint)(int ncid, int varid, const char *name, unsigned int *ip) = NULL;
int (*libnc_put_att_longlong)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const long long *op) = NULL;
int (*libnc_get_att_longlong)(int ncid, int varid, const char *name, long long *ip) = NULL;
int (*libnc_put_att_ulonglong)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned long long *op) = NULL;
int (*libnc_get_att_ulonglong)(int ncid, int varid, const char *name, unsigned long long *ip) = NULL;
int (*libnc_def_var)(int ncid, const char *name, nc_type xtype, int ndims, const int *dimidsp, int *varidp) = NULL;
int (*libnc_inq_var)(int ncid, int varid, char *name, nc_type * xtypep, int *ndimsp, int *dimidsp, int *nattsp) = NULL;
int (*libnc_inq_varid)(int ncid, const char *name, int *varidp) = NULL;
int (*libnc_inq_varname)(int ncid, int varid, char *name) = NULL;
int (*libnc_inq_vartype)(int ncid, int varid, nc_type * xtypep) = NULL;
int (*libnc_inq_varndims)(int ncid, int varid, int *ndimsp) = NULL;
int (*libnc_inq_vardimid)(int ncid, int varid, int *dimidsp) = NULL;
int (*libnc_inq_varnatts)(int ncid, int varid, int *nattsp) = NULL;
int (*libnc_rename_var)(int ncid, int varid, const char *name) = NULL;
int (*libnc_copy_var)(int ncid_in, int varid, int ncid_out) = NULL;
int (*libnc_put_var1_text)(int ncid, int varid, const size_t *indexp, const char *op) = NULL;
int (*libnc_get_var1_text)(int ncid, int varid, const size_t *indexp, char *ip) = NULL;
int (*libnc_put_var1_uchar)(int ncid, int varid, const size_t *indexp, const unsigned char *op) = NULL;
int (*libnc_get_var1_uchar)(int ncid, int varid, const size_t *indexp, unsigned char *ip) = NULL;
int (*libnc_put_var1_schar)(int ncid, int varid, const size_t *indexp, const signed char *op) = NULL;
int (*libnc_get_var1_schar)(int ncid, int varid, const size_t *indexp, signed char *ip) = NULL;
int (*libnc_put_var1_short)(int ncid, int varid, const size_t *indexp, const short *op) = NULL;
int (*libnc_get_var1_short)(int ncid, int varid, const size_t *indexp, short *ip) = NULL;
int (*libnc_put_var1_int)(int ncid, int varid, const size_t *indexp, const int *op) = NULL;
int (*libnc_get_var1_int)(int ncid, int varid, const size_t *indexp, int *ip) = NULL;
int (*libnc_put_var1_long)(int ncid, int varid, const size_t *indexp, const long *op) = NULL;
int (*libnc_get_var1_long)(int ncid, int varid, const size_t *indexp, long *ip) = NULL;
int (*libnc_put_var1_float)(int ncid, int varid, const size_t *indexp, const float *op) = NULL;
int (*libnc_get_var1_float)(int ncid, int varid, const size_t *indexp, float *ip) = NULL;
int (*libnc_put_var1_double)(int ncid, int varid, const size_t *indexp, const double *op) = NULL;
int (*libnc_get_var1_double)(int ncid, int varid, const size_t *indexp, double *ip) = NULL;
int (*libnc_put_var1_ushort)(int ncid, int varid, const size_t *indexp, const unsigned short *op) = NULL;
int (*libnc_get_var1_ushort)(int ncid, int varid, const size_t *indexp, unsigned short *ip) = NULL;
int (*libnc_put_var1_uint)(int ncid, int varid, const size_t *indexp, const unsigned int *op) = NULL;
int (*libnc_get_var1_uint)(int ncid, int varid, const size_t *indexp, unsigned int *ip) = NULL;
int (*libnc_put_var1_longlong)(int ncid, int varid, const size_t *indexp, const long long *op) = NULL;
int (*libnc_get_var1_longlong)(int ncid, int varid, const size_t *indexp, long long *ip) = NULL;
int (*libnc_put_var1_ulonglong)(int ncid, int varid, const size_t *indexp, const unsigned long long *op) = NULL;
int (*libnc_get_var1_ulonglong)(int ncid, int varid, const size_t *indexp, unsigned long long *ip) = NULL;
int (*libnc_put_var1_string)(int ncid, int varid, const size_t *indexp, const char **op) = NULL;
int (*libnc_get_var1_string)(int ncid, int varid, const size_t *indexp, char **ip) = NULL;
int (*libnc_put_vara_text)(int ncid, int varid, const size_t *startp, const size_t *countp, const char *op) = NULL;
int (*libnc_get_vara_text)(int ncid, int varid, const size_t *startp, const size_t *countp, char *ip) = NULL;
int (*libnc_put_vara_uchar)(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned char *op) = NULL;
int (*libnc_get_vara_uchar)(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned char *ip) = NULL;
int (*libnc_put_vara_schar)(int ncid, int varid, const size_t *startp, const size_t *countp, const signed char *op) = NULL;
int (*libnc_get_vara_schar)(int ncid, int varid, const size_t *startp, const size_t *countp, signed char *ip) = NULL;
int (*libnc_put_vara_short)(int ncid, int varid, const size_t *startp, const size_t *countp, const short *op) = NULL;
int (*libnc_get_vara_short)(int ncid, int varid, const size_t *startp, const size_t *countp, short *ip) = NULL;
int (*libnc_put_vara_int)(int ncid, int varid, const size_t *startp, const size_t *countp, const int *op) = NULL;
int (*libnc_get_vara_int)(int ncid, int varid, const size_t *startp, const size_t *countp, int *ip) = NULL;
int (*libnc_put_vara_long)(int ncid, int varid, const size_t *startp, const size_t *countp, const long *op) = NULL;
int (*libnc_get_vara_long)(int ncid, int varid, const size_t *startp, const size_t *countp, long *ip) = NULL;
int (*libnc_put_vara_float)(int ncid, int varid, const size_t *startp, const size_t *countp, const float *op) = NULL;
int (*libnc_get_vara_float)(int ncid, int varid, const size_t *startp, const size_t *countp, float *ip) = NULL;
int (*libnc_put_vara_double)(int ncid, int varid, const size_t *startp, const size_t *countp, const double *op) = NULL;
int (*libnc_get_vara_double)(int ncid, int varid, const size_t *startp, const size_t *countp, double *ip) = NULL;
int (*libnc_put_vara_ushort)(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned short *op) = NULL;
int (*libnc_get_vara_ushort)(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned short *ip) = NULL;
int (*libnc_put_vara_uint)(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned int *op) = NULL;
int (*libnc_get_vara_uint)(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned int *ip) = NULL;
int (*libnc_put_vara_longlong)(int ncid, int varid, const size_t *startp, const size_t *countp, const long long *op) = NULL;
int (*libnc_get_vara_longlong)(int ncid, int varid, const size_t *startp, const size_t *countp, long long *ip) = NULL;
int (*libnc_put_vara_ulonglong)(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned long long *op) = NULL;
int (*libnc_get_vara_ulonglong)(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned long long *ip) = NULL;
int (*libnc_put_vara_string)(int ncid, int varid, const size_t *startp, const size_t *countp, const char **op) = NULL;
int (*libnc_get_vara_string)(int ncid, int varid, const size_t *startp, const size_t *countp, char **ip) = NULL;
int (*libnc_put_vars_text)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const char *op) = NULL;
int (*libnc_get_vars_text)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, char *ip) = NULL;
int (*libnc_put_vars_uchar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned char *op) = NULL;
int (*libnc_get_vars_uchar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned char *ip) = NULL;
int (*libnc_put_vars_schar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const signed char *op) = NULL;
int (*libnc_get_vars_schar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, signed char *ip) = NULL;
int (*libnc_put_vars_short)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const short *op) = NULL;
int (*libnc_get_vars_short)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, short *ip) = NULL;
int (*libnc_put_vars_int)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const int *op) = NULL;
int (*libnc_get_vars_int)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, int *ip) = NULL;
int (*libnc_put_vars_long)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const long *op) = NULL;
int (*libnc_get_vars_long)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, long *ip) = NULL;
int (*libnc_put_vars_float)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const float *op) = NULL;
int (*libnc_get_vars_float)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, float *ip) = NULL;
int (*libnc_put_vars_double)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const double *op) = NULL;
int (*libnc_get_vars_double)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, double *ip) = NULL;
int (*libnc_put_vars_ushort)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned short *op) = NULL;
int (*libnc_get_vars_ushort)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned short *ip) = NULL;
int (*libnc_put_vars_uint)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned int *op) = NULL;
int (*libnc_get_vars_uint)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned int *ip) = NULL;
int (*libnc_put_vars_longlong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const long long *op) = NULL;
int (*libnc_get_vars_longlong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, long long *ip) = NULL;
int (*libnc_put_vars_ulonglong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned long long *op) = NULL;
int (*libnc_get_vars_ulonglong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned long long *ip) = NULL;
int (*libnc_put_vars_string)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const char **op) = NULL;
int (*libnc_get_vars_string)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, char **ip) = NULL;
int (*libnc_put_varm_text)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const char *op) = NULL;
int (*libnc_get_varm_text)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, char *ip) = NULL;
int (*libnc_put_varm_uchar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned char *op) = NULL;
int (*libnc_get_varm_uchar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned char *ip) = NULL;
int (*libnc_put_varm_schar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const signed char *op) = NULL;
int (*libnc_get_varm_schar)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, signed char *ip) = NULL;
int (*libnc_put_varm_short)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const short *op) = NULL;
int (*libnc_get_varm_short)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, short *ip) = NULL;
int (*libnc_put_varm_int)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const int *op) = NULL;
int (*libnc_get_varm_int)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, int *ip) = NULL;
int (*libnc_put_varm_long)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const long *op) = NULL;
int (*libnc_get_varm_long)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, long *ip) = NULL;
int (*libnc_put_varm_float)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const float *op) = NULL;
int (*libnc_get_varm_float)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, float *ip) = NULL;
int (*libnc_put_varm_double)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const double *op) = NULL;
int (*libnc_get_varm_double)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, double *ip) = NULL;
int (*libnc_put_varm_ushort)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned short *op) = NULL;
int (*libnc_get_varm_ushort)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned short *ip) = NULL;
int (*libnc_put_varm_uint)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned int *op) = NULL;
int (*libnc_get_varm_uint)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned int *ip) = NULL;
int (*libnc_put_varm_longlong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const long long *op) = NULL;
int (*libnc_get_varm_longlong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, long long *ip) = NULL;
int (*libnc_put_varm_ulonglong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned long long *op) = NULL;
int (*libnc_get_varm_ulonglong)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned long long *ip) = NULL;
int (*libnc_put_varm_string)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const char **op) = NULL;
int (*libnc_get_varm_string)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, char **ip) = NULL;
int (*libnc_put_var_text)(int ncid, int varid, const char *op) = NULL;
int (*libnc_get_var_text)(int ncid, int varid, char *ip) = NULL;
int (*libnc_put_var_uchar)(int ncid, int varid, const unsigned char *op) = NULL;
int (*libnc_get_var_uchar)(int ncid, int varid, unsigned char *ip) = NULL;
int (*libnc_put_var_schar)(int ncid, int varid, const signed char *op) = NULL;
int (*libnc_get_var_schar)(int ncid, int varid, signed char *ip) = NULL;
int (*libnc_put_var_short)(int ncid, int varid, const short *op) = NULL;
int (*libnc_get_var_short)(int ncid, int varid, short *ip) = NULL;
int (*libnc_put_var_int)(int ncid, int varid, const int *op) = NULL;
int (*libnc_get_var_int)(int ncid, int varid, int *ip) = NULL;
int (*libnc_put_var_long)(int ncid, int varid, const long *op) = NULL;
int (*libnc_get_var_long)(int ncid, int varid, long *ip) = NULL;
int (*libnc_put_var_float)(int ncid, int varid, const float *op) = NULL;
int (*libnc_get_var_float)(int ncid, int varid, float *ip) = NULL;
int (*libnc_put_var_double)(int ncid, int varid, const double *op) = NULL;
int (*libnc_get_var_double)(int ncid, int varid, double *ip) = NULL;
int (*libnc_put_var_ushort)(int ncid, int varid, const unsigned short *op) = NULL;
int (*libnc_get_var_ushort)(int ncid, int varid, unsigned short *ip) = NULL;
int (*libnc_put_var_uint)(int ncid, int varid, const unsigned int *op) = NULL;
int (*libnc_get_var_uint)(int ncid, int varid, unsigned int *ip) = NULL;
int (*libnc_put_var_longlong)(int ncid, int varid, const long long *op) = NULL;
int (*libnc_get_var_longlong)(int ncid, int varid, long long *ip) = NULL;
int (*libnc_put_var_ulonglong)(int ncid, int varid, const unsigned long long *op) = NULL;
int (*libnc_get_var_ulonglong)(int ncid, int varid, unsigned long long *ip) = NULL;
int (*libnc_put_var_string)(int ncid, int varid, const char **op) = NULL;
int (*libnc_get_var_string)(int ncid, int varid, char **ip) = NULL;
int (*libnc_reclaim_data)(int ncid, nc_type xtypeid, void *memory, size_t count) = NULL;
int (*libnc_reclaim_data_all)(int ncid, nc_type xtypeid, void *memory, size_t count) = NULL;
int (*libnc_copy_data)(int ncid, nc_type xtypeid, const void *memory, size_t count, void *copy) = NULL;
int (*libnc_copy_data_all)(int ncid, nc_type xtypeid, const void *memory, size_t count, void **copyp) = NULL;
int (*libnc_dump_data)(int ncid, nc_type xtypeid, void *memory, size_t count, char **buf) = NULL;
int (*libnc_put_att_ubyte)(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned char *op) = NULL;
int (*libnc_get_att_ubyte)(int ncid, int varid, const char *name, unsigned char *ip) = NULL;
int (*libnc_put_var1_ubyte)(int ncid, int varid, const size_t *indexp, const unsigned char *op) = NULL;
int (*libnc_get_var1_ubyte)(int ncid, int varid, const size_t *indexp, unsigned char *ip) = NULL;
int (*libnc_put_vara_ubyte)(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned char *op) = NULL;
int (*libnc_get_vara_ubyte)(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned char *ip) = NULL;
int (*libnc_put_vars_ubyte)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned char *op) = NULL;
int (*libnc_get_vars_ubyte)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned char *ip) = NULL;
int (*libnc_put_varm_ubyte)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned char *op) = NULL;
int (*libnc_get_varm_ubyte)(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned char *ip) = NULL;
int (*libnc_put_var_ubyte)(int ncid, int varid, const unsigned char *op) = NULL;
int (*libnc_get_var_ubyte)(int ncid, int varid, unsigned char *ip) = NULL;
int (*libnc_set_log_level)(int new_level) = NULL;
int (*libnc_show_metadata)(int ncid) = NULL;
int (*libnc_delete)(const char *path) = NULL;
int (*libnc__create_mp)(const char *path, int cmode, size_t initialsz, int basepe, size_t *chunksizehintp, int *ncidp) = NULL;
int (*libnc__open_mp)(const char *path, int mode, int basepe, size_t *chunksizehintp, int *ncidp) = NULL;
int (*libnc_delete_mp)(const char *path, int basepe) = NULL;
int (*libnc_set_base_pe)(int ncid, int pe) = NULL;
int (*libnc_inq_base_pe)(int ncid, int *pe) = NULL;
int (*libnctypelen)(nc_type datatype) = NULL;
int (*libnccreate)(const char *path, int cmode) = NULL;
int (*libncopen)(const char *path, int mode) = NULL;
int (*libncsetfill)(int ncid, int fillmode) = NULL;
int (*libncredef)(int ncid) = NULL;
int (*libncendef)(int ncid) = NULL;
int (*libncsync)(int ncid) = NULL;
int (*libncabort)(int ncid) = NULL;
int (*libncclose)(int ncid) = NULL;
int (*libncinquire)(int ncid, int *ndimsp, int *nvarsp, int *nattsp, int *unlimdimp) = NULL;
int (*libncdimdef)(int ncid, const char *name, long len) = NULL;
int (*libncdimid)(int ncid, const char *name) = NULL;
int (*libncdiminq)(int ncid, int dimid, char *name, long *lenp) = NULL;
int (*libncdimrename)(int ncid, int dimid, const char *name) = NULL;
int (*libncattput)(int ncid, int varid, const char *name, nc_type xtype, int len, const void *op) = NULL;
int (*libncattinq)(int ncid, int varid, const char *name, nc_type * xtypep, int *lenp) = NULL;
int (*libncattget)(int ncid, int varid, const char *name, void *ip) = NULL;
int (*libncattcopy)(int ncid_in, int varid_in, const char *name, int ncid_out, int varid_out) = NULL;
int (*libncattname)(int ncid, int varid, int attnum, char *name) = NULL;
int (*libncattrename)(int ncid, int varid, const char *name, const char *newname) = NULL;
int (*libncattdel)(int ncid, int varid, const char *name) = NULL;
int (*libncvardef)(int ncid, const char *name, nc_type xtype, int ndims, const int *dimidsp) = NULL;
int (*libncvarid)(int ncid, const char *name) = NULL;
int (*libncvarinq)(int ncid, int varid, char *name, nc_type * xtypep, int *ndimsp, int *dimidsp, int *nattsp) = NULL;
int (*libncvarput1)(int ncid, int varid, const long *indexp, const void *op) = NULL;
int (*libncvarget1)(int ncid, int varid, const long *indexp, void *ip) = NULL;
int (*libncvarput)(int ncid, int varid, const long *startp, const long *countp, const void *op) = NULL;
int (*libncvarget)(int ncid, int varid, const long *startp, const long *countp, void *ip) = NULL;
int (*libncvarputs)(int ncid, int varid, const long *startp, const long *countp, const long *stridep, const void *op) = NULL;
int (*libncvargets)(int ncid, int varid, const long *startp, const long *countp, const long *stridep, void *ip) = NULL;
int (*libncvarputg)(int ncid, int varid, const long *startp, const long *countp, const long *stridep, const long *imapp, const void *op) = NULL;
int (*libncvargetg)(int ncid, int varid, const long *startp, const long *countp, const long *stridep, const long *imapp, void *ip) = NULL;
int (*libncvarrename)(int ncid, int varid, const char *name) = NULL;
int (*libncrecinq)(int ncid, int *nrecvarsp, int *recvaridsp, long *recsizesp) = NULL;
int (*libncrecget)(int ncid, long recnum, void **datap) = NULL;
int (*libncrecput)(int ncid, long recnum, void *const *datap) = NULL;
int (*libnc_initialize)(void) = NULL;
int (*libnc_finalize)(void) = NULL;
int (*libnc_rc_set)(const char *key, const char *value) = NULL;


/* Wrapper functions */



int nc_def_user_format(int mode_flag, NC_Dispatch * dispatch_table, char *magic_number) {
  FUNCTION_ENTRY;
  int ret = libnc_def_user_format(mode_flag, dispatch_table, magic_number);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_user_format(int mode_flag, NC_Dispatch ** dispatch_table, char *magic_number) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_user_format(mode_flag, dispatch_table, magic_number);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_alignment(int threshold, int alignment) {
  FUNCTION_ENTRY;
  int ret = libnc_set_alignment(threshold, alignment);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_alignment(int *thresholdp, int *alignmentp) {
  FUNCTION_ENTRY;
  int ret = libnc_get_alignment(thresholdp, alignmentp);
  FUNCTION_EXIT;
  return ret;
}



int nc__create(const char *path, int cmode, size_t initialsz, size_t *chunksizehintp, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libnc__create(path, cmode, initialsz, chunksizehintp, ncidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_create(const char *path, int cmode, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libnc_create(path, cmode, ncidp);
  FUNCTION_EXIT;
  return ret;
}



int nc__open(const char *path, int mode, size_t *chunksizehintp, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libnc__open(path, mode, chunksizehintp, ncidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_open(const char *path, int mode, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libnc_open(path, mode, ncidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_path(int ncid, size_t *pathlen, char *path) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_path(ncid, pathlen, path);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_ncid(int ncid, const char *name, int *grp_ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_ncid(ncid, name, grp_ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grps(int ncid, int *numgrps, int *ncids) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grps(ncid, numgrps, ncids);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grpname(int ncid, char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grpname(ncid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grpname_full(int ncid, size_t *lenp, char *full_name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grpname_full(ncid, lenp, full_name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grpname_len(int ncid, size_t *lenp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grpname_len(ncid, lenp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grp_parent(int ncid, int *parent_ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grp_parent(ncid, parent_ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grp_ncid(int ncid, const char *grp_name, int *grp_ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grp_ncid(ncid, grp_name, grp_ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_grp_full_ncid(int ncid, const char *full_name, int *grp_ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_grp_full_ncid(ncid, full_name, grp_ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_varids(int ncid, int *nvars, int *varids) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_varids(ncid, nvars, varids);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_dimids(int ncid, int *ndims, int *dimids, int include_parents) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_dimids(ncid, ndims, dimids, include_parents);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_typeids(int ncid, int *ntypes, int *typeids) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_typeids(ncid, ntypes, typeids);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_type_equal(int ncid1, nc_type typeid1, int ncid2, nc_type typeid2, int *equal) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_type_equal(ncid1, typeid1, ncid2, typeid2, equal);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_grp(int parent_ncid, const char *name, int *new_ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_def_grp(parent_ncid, name, new_ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_rename_grp(int grpid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_rename_grp(grpid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_compound(int ncid, size_t size, const char *name, nc_type * typeidp) {
  FUNCTION_ENTRY;
  int ret = libnc_def_compound(ncid, size, name, typeidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_insert_compound(int ncid, nc_type xtype, const char *name, size_t offset, nc_type field_typeid) {
  FUNCTION_ENTRY;
  int ret = libnc_insert_compound(ncid, xtype, name, offset, field_typeid);
  FUNCTION_EXIT;
  return ret;
}



int nc_insert_array_compound(int ncid, nc_type xtype, const char *name, size_t offset, nc_type field_typeid, int ndims, const int *dim_sizes) {
  FUNCTION_ENTRY;
  int ret = libnc_insert_array_compound(ncid, xtype, name, offset, field_typeid, ndims, dim_sizes);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_type(int ncid, nc_type xtype, char *name, size_t *size) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_type(ncid, xtype, name, size);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_typeid(int ncid, const char *name, nc_type * typeidp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_typeid(ncid, name, typeidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound(int ncid, nc_type xtype, char *name, size_t *sizep, size_t *nfieldsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound(ncid, xtype, name, sizep, nfieldsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_name(int ncid, nc_type xtype, char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_name(ncid, xtype, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_size(int ncid, nc_type xtype, size_t *sizep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_size(ncid, xtype, sizep);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_nfields(int ncid, nc_type xtype, size_t *nfieldsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_nfields(ncid, xtype, nfieldsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_field(int ncid, nc_type xtype, int fieldid, char *name, size_t *offsetp, nc_type * field_typeidp, int *ndimsp, int *dim_sizesp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_field(ncid, xtype, fieldid, name, offsetp, field_typeidp, ndimsp, dim_sizesp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_fieldname(int ncid, nc_type xtype, int fieldid, char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_fieldname(ncid, xtype, fieldid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_fieldindex(int ncid, nc_type xtype, const char *name, int *fieldidp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_fieldindex(ncid, xtype, name, fieldidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_fieldoffset(int ncid, nc_type xtype, int fieldid, size_t *offsetp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_fieldoffset(ncid, xtype, fieldid, offsetp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_fieldtype(int ncid, nc_type xtype, int fieldid, nc_type * field_typeidp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_fieldtype(ncid, xtype, fieldid, field_typeidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_fieldndims(int ncid, nc_type xtype, int fieldid, int *ndimsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_fieldndims(ncid, xtype, fieldid, ndimsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_compound_fielddim_sizes(int ncid, nc_type xtype, int fieldid, int *dim_sizes) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_compound_fielddim_sizes(ncid, xtype, fieldid, dim_sizes);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_vlen(int ncid, const char *name, nc_type base_typeid, nc_type * xtypep) {
  FUNCTION_ENTRY;
  int ret = libnc_def_vlen(ncid, name, base_typeid, xtypep);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_vlen(int ncid, nc_type xtype, char *name, size_t *datum_sizep, nc_type * base_nc_typep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_vlen(ncid, xtype, name, datum_sizep, base_nc_typep);
  FUNCTION_EXIT;
  return ret;
}



int nc_free_vlen(nc_vlen_t * vl) {
  FUNCTION_ENTRY;
  int ret = libnc_free_vlen(vl);
  FUNCTION_EXIT;
  return ret;
}



int nc_free_vlens(size_t len, nc_vlen_t vlens[]) {
  FUNCTION_ENTRY;
  int ret = libnc_free_vlens(len, vlens);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vlen_element(int ncid, int typeid1, void *vlen_element, size_t len, const void *data) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vlen_element(ncid, typeid1, vlen_element, len, data);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vlen_element(int ncid, int typeid1, const void *vlen_element, size_t *len, void *data) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vlen_element(ncid, typeid1, vlen_element, len, data);
  FUNCTION_EXIT;
  return ret;
}



int nc_free_string(size_t len, char **data) {
  FUNCTION_ENTRY;
  int ret = libnc_free_string(len, data);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_user_type(int ncid, nc_type xtype, char *name, size_t *size, nc_type * base_nc_typep, size_t *nfieldsp, int *classp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_user_type(ncid, xtype, name, size, base_nc_typep, nfieldsp, classp);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att(int ncid, int varid, const char *name, nc_type xtype, size_t len, const void *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att(int ncid, int varid, const char *name, void *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_enum(int ncid, nc_type base_typeid, const char *name, nc_type * typeidp) {
  FUNCTION_ENTRY;
  int ret = libnc_def_enum(ncid, base_typeid, name, typeidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_insert_enum(int ncid, nc_type xtype, const char *name, const void *value) {
  FUNCTION_ENTRY;
  int ret = libnc_insert_enum(ncid, xtype, name, value);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_enum(int ncid, nc_type xtype, char *name, nc_type * base_nc_typep, size_t *base_sizep, size_t *num_membersp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_enum(ncid, xtype, name, base_nc_typep, base_sizep, num_membersp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_enum_member(int ncid, nc_type xtype, int idx, char *name, void *value) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_enum_member(ncid, xtype, idx, name, value);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_enum_ident(int ncid, nc_type xtype, long long value, char *identifier) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_enum_ident(ncid, xtype, value, identifier);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_opaque(int ncid, size_t size, const char *name, nc_type * xtypep) {
  FUNCTION_ENTRY;
  int ret = libnc_def_opaque(ncid, size, name, xtypep);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_opaque(int ncid, nc_type xtype, char *name, size_t *sizep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_opaque(ncid, xtype, name, sizep);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var(int ncid, int varid, const void *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var(int ncid, int varid, void *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1(int ncid, int varid, const size_t *indexp, const void *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1(int ncid, int varid, const size_t *indexp, void *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara(int ncid, int varid, const size_t *startp, const size_t *countp, const void *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara(int ncid, int varid, const size_t *startp, const size_t *countp, void *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const void *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, void *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const void *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, void *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_quantize(int ncid, int varid, int quantize_mode, int nsd) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_quantize(ncid, varid, quantize_mode, nsd);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_quantize(int ncid, int varid, int *quantize_modep, int *nsdp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_quantize(ncid, varid, quantize_modep, nsdp);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_deflate(int ncid, int varid, int shuffle, int deflate, int deflate_level) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_deflate(ncid, varid, shuffle, deflate, deflate_level);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_deflate(int ncid, int varid, int *shufflep, int *deflatep, int *deflate_levelp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_deflate(ncid, varid, shufflep, deflatep, deflate_levelp);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_szip(int ncid, int varid, int options_mask, int pixels_per_block) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_szip(ncid, varid, options_mask, pixels_per_block);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_szip(int ncid, int varid, int *options_maskp, int *pixels_per_blockp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_szip(ncid, varid, options_maskp, pixels_per_blockp);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_fletcher32(int ncid, int varid, int fletcher32) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_fletcher32(ncid, varid, fletcher32);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_fletcher32(int ncid, int varid, int *fletcher32p) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_fletcher32(ncid, varid, fletcher32p);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_chunking(int ncid, int varid, int storage, const size_t *chunksizesp) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_chunking(ncid, varid, storage, chunksizesp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_chunking(int ncid, int varid, int *storagep, size_t *chunksizesp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_chunking(ncid, varid, storagep, chunksizesp);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_fill(int ncid, int varid, int no_fill, const void *fill_value) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_fill(ncid, varid, no_fill, fill_value);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_fill(int ncid, int varid, int *no_fill, void *fill_valuep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_fill(ncid, varid, no_fill, fill_valuep);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_endian(int ncid, int varid, int endian) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_endian(ncid, varid, endian);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_endian(int ncid, int varid, int *endianp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_endian(ncid, varid, endianp);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var_filter(int ncid, int varid, unsigned int id, size_t nparams, const unsigned int *parms) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var_filter(ncid, varid, id, nparams, parms);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var_filter(int ncid, int varid, unsigned int *idp, size_t *nparams, unsigned int *params) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var_filter(ncid, varid, idp, nparams, params);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_fill(int ncid, int fillmode, int *old_modep) {
  FUNCTION_ENTRY;
  int ret = libnc_set_fill(ncid, fillmode, old_modep);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_default_format(int format, int *old_formatp) {
  FUNCTION_ENTRY;
  int ret = libnc_set_default_format(format, old_formatp);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_chunk_cache(size_t size, size_t nelems, float preemption) {
  FUNCTION_ENTRY;
  int ret = libnc_set_chunk_cache(size, nelems, preemption);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_chunk_cache(size_t *sizep, size_t *nelemsp, float *preemptionp) {
  FUNCTION_ENTRY;
  int ret = libnc_get_chunk_cache(sizep, nelemsp, preemptionp);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_var_chunk_cache(int ncid, int varid, size_t size, size_t nelems, float preemption) {
  FUNCTION_ENTRY;
  int ret = libnc_set_var_chunk_cache(ncid, varid, size, nelems, preemption);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_chunk_cache(int ncid, int varid, size_t *sizep, size_t *nelemsp, float *preemptionp) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_chunk_cache(ncid, varid, sizep, nelemsp, preemptionp);
  FUNCTION_EXIT;
  return ret;
}



int nc_redef(int ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_redef(ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc__enddef(int ncid, size_t h_minfree, size_t v_align, size_t v_minfree, size_t r_align) {
  FUNCTION_ENTRY;
  int ret = libnc__enddef(ncid, h_minfree, v_align, v_minfree, r_align);
  FUNCTION_EXIT;
  return ret;
}



int nc_enddef(int ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_enddef(ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_sync(int ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_sync(ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_abort(int ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_abort(ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_close(int ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_close(ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq(int ncid, int *ndimsp, int *nvarsp, int *nattsp, int *unlimdimidp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq(ncid, ndimsp, nvarsp, nattsp, unlimdimidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_ndims(int ncid, int *ndimsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_ndims(ncid, ndimsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_nvars(int ncid, int *nvarsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_nvars(ncid, nvarsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_natts(int ncid, int *nattsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_natts(ncid, nattsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_unlimdim(int ncid, int *unlimdimidp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_unlimdim(ncid, unlimdimidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_unlimdims(int ncid, int *nunlimdimsp, int *unlimdimidsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_unlimdims(ncid, nunlimdimsp, unlimdimidsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_format(int ncid, int *formatp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_format(ncid, formatp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_format_extended(int ncid, int *formatp, int *modep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_format_extended(ncid, formatp, modep);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_dim(int ncid, const char *name, size_t len, int *idp) {
  FUNCTION_ENTRY;
  int ret = libnc_def_dim(ncid, name, len, idp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_dimid(int ncid, const char *name, int *idp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_dimid(ncid, name, idp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_dim(int ncid, int dimid, char *name, size_t *lenp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_dim(ncid, dimid, name, lenp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_dimname(int ncid, int dimid, char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_dimname(ncid, dimid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_dimlen(int ncid, int dimid, size_t *lenp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_dimlen(ncid, dimid, lenp);
  FUNCTION_EXIT;
  return ret;
}



int nc_rename_dim(int ncid, int dimid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_rename_dim(ncid, dimid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_att(int ncid, int varid, const char *name, nc_type * xtypep, size_t *lenp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_att(ncid, varid, name, xtypep, lenp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_attid(int ncid, int varid, const char *name, int *idp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_attid(ncid, varid, name, idp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_atttype(int ncid, int varid, const char *name, nc_type * xtypep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_atttype(ncid, varid, name, xtypep);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_attlen(int ncid, int varid, const char *name, size_t *lenp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_attlen(ncid, varid, name, lenp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_attname(int ncid, int varid, int attnum, char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_attname(ncid, varid, attnum, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_copy_att(int ncid_in, int varid_in, const char *name, int ncid_out, int varid_out) {
  FUNCTION_ENTRY;
  int ret = libnc_copy_att(ncid_in, varid_in, name, ncid_out, varid_out);
  FUNCTION_EXIT;
  return ret;
}



int nc_rename_att(int ncid, int varid, const char *name, const char *newname) {
  FUNCTION_ENTRY;
  int ret = libnc_rename_att(ncid, varid, name, newname);
  FUNCTION_EXIT;
  return ret;
}



int nc_del_att(int ncid, int varid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_del_att(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_text(int ncid, int varid, const char *name, size_t len, const char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_text(ncid, varid, name, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_text(int ncid, int varid, const char *name, char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_text(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_string(int ncid, int varid, const char *name, size_t len, const char **op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_string(ncid, varid, name, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_string(int ncid, int varid, const char *name, char **ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_string(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_uchar(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_uchar(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_uchar(int ncid, int varid, const char *name, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_uchar(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_schar(int ncid, int varid, const char *name, nc_type xtype, size_t len, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_schar(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_schar(int ncid, int varid, const char *name, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_schar(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_short(int ncid, int varid, const char *name, nc_type xtype, size_t len, const short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_short(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_short(int ncid, int varid, const char *name, short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_short(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_int(int ncid, int varid, const char *name, nc_type xtype, size_t len, const int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_int(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_int(int ncid, int varid, const char *name, int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_int(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_long(int ncid, int varid, const char *name, nc_type xtype, size_t len, const long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_long(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_long(int ncid, int varid, const char *name, long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_long(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_float(int ncid, int varid, const char *name, nc_type xtype, size_t len, const float *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_float(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_float(int ncid, int varid, const char *name, float *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_float(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_double(int ncid, int varid, const char *name, nc_type xtype, size_t len, const double *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_double(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_double(int ncid, int varid, const char *name, double *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_double(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_ushort(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_ushort(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_ushort(int ncid, int varid, const char *name, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_ushort(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_uint(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_uint(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_uint(int ncid, int varid, const char *name, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_uint(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_longlong(int ncid, int varid, const char *name, nc_type xtype, size_t len, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_longlong(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_longlong(int ncid, int varid, const char *name, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_longlong(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_ulonglong(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_ulonglong(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_ulonglong(int ncid, int varid, const char *name, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_ulonglong(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_def_var(int ncid, const char *name, nc_type xtype, int ndims, const int *dimidsp, int *varidp) {
  FUNCTION_ENTRY;
  int ret = libnc_def_var(ncid, name, xtype, ndims, dimidsp, varidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_var(int ncid, int varid, char *name, nc_type * xtypep, int *ndimsp, int *dimidsp, int *nattsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_var(ncid, varid, name, xtypep, ndimsp, dimidsp, nattsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_varid(int ncid, const char *name, int *varidp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_varid(ncid, name, varidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_varname(int ncid, int varid, char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_varname(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_vartype(int ncid, int varid, nc_type * xtypep) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_vartype(ncid, varid, xtypep);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_varndims(int ncid, int varid, int *ndimsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_varndims(ncid, varid, ndimsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_vardimid(int ncid, int varid, int *dimidsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_vardimid(ncid, varid, dimidsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_varnatts(int ncid, int varid, int *nattsp) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_varnatts(ncid, varid, nattsp);
  FUNCTION_EXIT;
  return ret;
}



int nc_rename_var(int ncid, int varid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libnc_rename_var(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}



int nc_copy_var(int ncid_in, int varid, int ncid_out) {
  FUNCTION_ENTRY;
  int ret = libnc_copy_var(ncid_in, varid, ncid_out);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_text(int ncid, int varid, const size_t *indexp, const char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_text(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_text(int ncid, int varid, const size_t *indexp, char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_text(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_uchar(int ncid, int varid, const size_t *indexp, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_uchar(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_uchar(int ncid, int varid, const size_t *indexp, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_uchar(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_schar(int ncid, int varid, const size_t *indexp, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_schar(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_schar(int ncid, int varid, const size_t *indexp, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_schar(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_short(int ncid, int varid, const size_t *indexp, const short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_short(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_short(int ncid, int varid, const size_t *indexp, short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_short(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_int(int ncid, int varid, const size_t *indexp, const int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_int(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_int(int ncid, int varid, const size_t *indexp, int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_int(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_long(int ncid, int varid, const size_t *indexp, const long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_long(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_long(int ncid, int varid, const size_t *indexp, long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_long(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_float(int ncid, int varid, const size_t *indexp, const float *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_float(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_float(int ncid, int varid, const size_t *indexp, float *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_float(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_double(int ncid, int varid, const size_t *indexp, const double *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_double(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_double(int ncid, int varid, const size_t *indexp, double *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_double(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_ushort(int ncid, int varid, const size_t *indexp, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_ushort(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_ushort(int ncid, int varid, const size_t *indexp, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_ushort(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_uint(int ncid, int varid, const size_t *indexp, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_uint(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_uint(int ncid, int varid, const size_t *indexp, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_uint(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_longlong(int ncid, int varid, const size_t *indexp, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_longlong(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_longlong(int ncid, int varid, const size_t *indexp, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_longlong(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_ulonglong(int ncid, int varid, const size_t *indexp, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_ulonglong(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_ulonglong(int ncid, int varid, const size_t *indexp, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_ulonglong(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_string(int ncid, int varid, const size_t *indexp, const char **op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_string(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_string(int ncid, int varid, const size_t *indexp, char **ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_string(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_text(int ncid, int varid, const size_t *startp, const size_t *countp, const char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_text(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_text(int ncid, int varid, const size_t *startp, const size_t *countp, char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_text(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_uchar(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_uchar(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_uchar(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_uchar(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_schar(int ncid, int varid, const size_t *startp, const size_t *countp, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_schar(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_schar(int ncid, int varid, const size_t *startp, const size_t *countp, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_schar(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_short(int ncid, int varid, const size_t *startp, const size_t *countp, const short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_short(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_short(int ncid, int varid, const size_t *startp, const size_t *countp, short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_short(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_int(int ncid, int varid, const size_t *startp, const size_t *countp, const int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_int(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_int(int ncid, int varid, const size_t *startp, const size_t *countp, int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_int(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_long(int ncid, int varid, const size_t *startp, const size_t *countp, const long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_long(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_long(int ncid, int varid, const size_t *startp, const size_t *countp, long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_long(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_float(int ncid, int varid, const size_t *startp, const size_t *countp, const float *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_float(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_float(int ncid, int varid, const size_t *startp, const size_t *countp, float *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_float(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_double(int ncid, int varid, const size_t *startp, const size_t *countp, const double *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_double(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_double(int ncid, int varid, const size_t *startp, const size_t *countp, double *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_double(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_ushort(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_ushort(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_ushort(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_ushort(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_uint(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_uint(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_uint(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_uint(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_longlong(int ncid, int varid, const size_t *startp, const size_t *countp, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_longlong(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_longlong(int ncid, int varid, const size_t *startp, const size_t *countp, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_longlong(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_ulonglong(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_ulonglong(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_ulonglong(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_ulonglong(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_string(int ncid, int varid, const size_t *startp, const size_t *countp, const char **op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_string(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_string(int ncid, int varid, const size_t *startp, const size_t *countp, char **ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_string(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_text(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_text(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_text(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_text(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_uchar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_uchar(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_uchar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_uchar(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_schar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_schar(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_schar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_schar(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_short(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_short(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_short(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_short(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_int(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_int(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_int(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_int(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_long(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_long(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_long(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_long(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_float(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const float *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_float(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_float(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, float *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_float(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_double(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const double *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_double(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_double(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, double *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_double(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_ushort(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_ushort(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_ushort(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_ushort(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_uint(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_uint(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_uint(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_uint(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_longlong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_longlong(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_longlong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_longlong(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_ulonglong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_ulonglong(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_ulonglong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_ulonglong(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_string(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const char **op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_string(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_string(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, char **ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_string(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_text(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_text(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_text(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_text(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_uchar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_uchar(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_uchar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_uchar(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_schar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_schar(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_schar(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_schar(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_short(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_short(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_short(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_short(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_int(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_int(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_int(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_int(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_long(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_long(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_long(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_long(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_float(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const float *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_float(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_float(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, float *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_float(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_double(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const double *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_double(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_double(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, double *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_double(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_ushort(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_ushort(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_ushort(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_ushort(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_uint(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_uint(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_uint(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_uint(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_longlong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_longlong(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_longlong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_longlong(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_ulonglong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_ulonglong(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_ulonglong(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_ulonglong(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_string(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const char **op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_string(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_string(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, char **ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_string(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_text(int ncid, int varid, const char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_text(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_text(int ncid, int varid, char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_text(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_uchar(int ncid, int varid, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_uchar(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_uchar(int ncid, int varid, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_uchar(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_schar(int ncid, int varid, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_schar(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_schar(int ncid, int varid, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_schar(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_short(int ncid, int varid, const short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_short(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_short(int ncid, int varid, short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_short(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_int(int ncid, int varid, const int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_int(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_int(int ncid, int varid, int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_int(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_long(int ncid, int varid, const long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_long(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_long(int ncid, int varid, long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_long(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_float(int ncid, int varid, const float *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_float(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_float(int ncid, int varid, float *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_float(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_double(int ncid, int varid, const double *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_double(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_double(int ncid, int varid, double *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_double(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_ushort(int ncid, int varid, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_ushort(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_ushort(int ncid, int varid, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_ushort(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_uint(int ncid, int varid, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_uint(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_uint(int ncid, int varid, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_uint(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_longlong(int ncid, int varid, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_longlong(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_longlong(int ncid, int varid, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_longlong(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_ulonglong(int ncid, int varid, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_ulonglong(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_ulonglong(int ncid, int varid, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_ulonglong(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_string(int ncid, int varid, const char **op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_string(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_string(int ncid, int varid, char **ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_string(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_reclaim_data(int ncid, nc_type xtypeid, void *memory, size_t count) {
  FUNCTION_ENTRY;
  int ret = libnc_reclaim_data(ncid, xtypeid, memory, count);
  FUNCTION_EXIT;
  return ret;
}



int nc_reclaim_data_all(int ncid, nc_type xtypeid, void *memory, size_t count) {
  FUNCTION_ENTRY;
  int ret = libnc_reclaim_data_all(ncid, xtypeid, memory, count);
  FUNCTION_EXIT;
  return ret;
}



int nc_copy_data(int ncid, nc_type xtypeid, const void *memory, size_t count, void *copy) {
  FUNCTION_ENTRY;
  int ret = libnc_copy_data(ncid, xtypeid, memory, count, copy);
  FUNCTION_EXIT;
  return ret;
}



int nc_copy_data_all(int ncid, nc_type xtypeid, const void *memory, size_t count, void **copyp) {
  FUNCTION_ENTRY;
  int ret = libnc_copy_data_all(ncid, xtypeid, memory, count, copyp);
  FUNCTION_EXIT;
  return ret;
}



int nc_dump_data(int ncid, nc_type xtypeid, void *memory, size_t count, char **buf) {
  FUNCTION_ENTRY;
  int ret = libnc_dump_data(ncid, xtypeid, memory, count, buf);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_att_ubyte(int ncid, int varid, const char *name, nc_type xtype, size_t len, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_att_ubyte(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_att_ubyte(int ncid, int varid, const char *name, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_att_ubyte(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var1_ubyte(int ncid, int varid, const size_t *indexp, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var1_ubyte(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var1_ubyte(int ncid, int varid, const size_t *indexp, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var1_ubyte(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vara_ubyte(int ncid, int varid, const size_t *startp, const size_t *countp, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vara_ubyte(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vara_ubyte(int ncid, int varid, const size_t *startp, const size_t *countp, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vara_ubyte(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_vars_ubyte(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_vars_ubyte(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_vars_ubyte(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_vars_ubyte(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_varm_ubyte(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_varm_ubyte(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_varm_ubyte(int ncid, int varid, const size_t *startp, const size_t *countp, const ptrdiff_t *stridep, const ptrdiff_t *imapp, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_varm_ubyte(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_put_var_ubyte(int ncid, int varid, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libnc_put_var_ubyte(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}



int nc_get_var_ubyte(int ncid, int varid, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libnc_get_var_ubyte(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_log_level(int new_level) {
  FUNCTION_ENTRY;
  int ret = libnc_set_log_level(new_level);
  FUNCTION_EXIT;
  return ret;
}



int nc_show_metadata(int ncid) {
  FUNCTION_ENTRY;
  int ret = libnc_show_metadata(ncid);
  FUNCTION_EXIT;
  return ret;
}



int nc_delete(const char *path) {
  FUNCTION_ENTRY;
  int ret = libnc_delete(path);
  FUNCTION_EXIT;
  return ret;
}



int nc__create_mp(const char *path, int cmode, size_t initialsz, int basepe, size_t *chunksizehintp, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libnc__create_mp(path, cmode, initialsz, basepe, chunksizehintp, ncidp);
  FUNCTION_EXIT;
  return ret;
}



int nc__open_mp(const char *path, int mode, int basepe, size_t *chunksizehintp, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libnc__open_mp(path, mode, basepe, chunksizehintp, ncidp);
  FUNCTION_EXIT;
  return ret;
}



int nc_delete_mp(const char *path, int basepe) {
  FUNCTION_ENTRY;
  int ret = libnc_delete_mp(path, basepe);
  FUNCTION_EXIT;
  return ret;
}



int nc_set_base_pe(int ncid, int pe) {
  FUNCTION_ENTRY;
  int ret = libnc_set_base_pe(ncid, pe);
  FUNCTION_EXIT;
  return ret;
}



int nc_inq_base_pe(int ncid, int *pe) {
  FUNCTION_ENTRY;
  int ret = libnc_inq_base_pe(ncid, pe);
  FUNCTION_EXIT;
  return ret;
}



int nctypelen(nc_type datatype) {
  FUNCTION_ENTRY;
  int ret = libnctypelen(datatype);
  FUNCTION_EXIT;
  return ret;
}



int nccreate(const char *path, int cmode) {
  FUNCTION_ENTRY;
  int ret = libnccreate(path, cmode);
  FUNCTION_EXIT;
  return ret;
}



int ncopen(const char *path, int mode) {
  FUNCTION_ENTRY;
  int ret = libncopen(path, mode);
  FUNCTION_EXIT;
  return ret;
}



int ncsetfill(int ncid, int fillmode) {
  FUNCTION_ENTRY;
  int ret = libncsetfill(ncid, fillmode);
  FUNCTION_EXIT;
  return ret;
}



int ncredef(int ncid) {
  FUNCTION_ENTRY;
  int ret = libncredef(ncid);
  FUNCTION_EXIT;
  return ret;
}



int ncendef(int ncid) {
  FUNCTION_ENTRY;
  int ret = libncendef(ncid);
  FUNCTION_EXIT;
  return ret;
}



int ncsync(int ncid) {
  FUNCTION_ENTRY;
  int ret = libncsync(ncid);
  FUNCTION_EXIT;
  return ret;
}



int ncabort(int ncid) {
  FUNCTION_ENTRY;
  int ret = libncabort(ncid);
  FUNCTION_EXIT;
  return ret;
}



int ncclose(int ncid) {
  FUNCTION_ENTRY;
  int ret = libncclose(ncid);
  FUNCTION_EXIT;
  return ret;
}



int ncinquire(int ncid, int *ndimsp, int *nvarsp, int *nattsp, int *unlimdimp) {
  FUNCTION_ENTRY;
  int ret = libncinquire(ncid, ndimsp, nvarsp, nattsp, unlimdimp);
  FUNCTION_EXIT;
  return ret;
}



int ncdimdef(int ncid, const char *name, long len) {
  FUNCTION_ENTRY;
  int ret = libncdimdef(ncid, name, len);
  FUNCTION_EXIT;
  return ret;
}



int ncdimid(int ncid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncdimid(ncid, name);
  FUNCTION_EXIT;
  return ret;
}



int ncdiminq(int ncid, int dimid, char *name, long *lenp) {
  FUNCTION_ENTRY;
  int ret = libncdiminq(ncid, dimid, name, lenp);
  FUNCTION_EXIT;
  return ret;
}



int ncdimrename(int ncid, int dimid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncdimrename(ncid, dimid, name);
  FUNCTION_EXIT;
  return ret;
}



int ncattput(int ncid, int varid, const char *name, nc_type xtype, int len, const void *op) {
  FUNCTION_ENTRY;
  int ret = libncattput(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}



int ncattinq(int ncid, int varid, const char *name, nc_type * xtypep, int *lenp) {
  FUNCTION_ENTRY;
  int ret = libncattinq(ncid, varid, name, xtypep, lenp);
  FUNCTION_EXIT;
  return ret;
}



int ncattget(int ncid, int varid, const char *name, void *ip) {
  FUNCTION_ENTRY;
  int ret = libncattget(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}



int ncattcopy(int ncid_in, int varid_in, const char *name, int ncid_out, int varid_out) {
  FUNCTION_ENTRY;
  int ret = libncattcopy(ncid_in, varid_in, name, ncid_out, varid_out);
  FUNCTION_EXIT;
  return ret;
}



int ncattname(int ncid, int varid, int attnum, char *name) {
  FUNCTION_ENTRY;
  int ret = libncattname(ncid, varid, attnum, name);
  FUNCTION_EXIT;
  return ret;
}



int ncattrename(int ncid, int varid, const char *name, const char *newname) {
  FUNCTION_ENTRY;
  int ret = libncattrename(ncid, varid, name, newname);
  FUNCTION_EXIT;
  return ret;
}



int ncattdel(int ncid, int varid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncattdel(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}



int ncvardef(int ncid, const char *name, nc_type xtype, int ndims, const int *dimidsp) {
  FUNCTION_ENTRY;
  int ret = libncvardef(ncid, name, xtype, ndims, dimidsp);
  FUNCTION_EXIT;
  return ret;
}



int ncvarid(int ncid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncvarid(ncid, name);
  FUNCTION_EXIT;
  return ret;
}



int ncvarinq(int ncid, int varid, char *name, nc_type * xtypep, int *ndimsp, int *dimidsp, int *nattsp) {
  FUNCTION_ENTRY;
  int ret = libncvarinq(ncid, varid, name, xtypep, ndimsp, dimidsp, nattsp);
  FUNCTION_EXIT;
  return ret;
}



int ncvarput1(int ncid, int varid, const long *indexp, const void *op) {
  FUNCTION_ENTRY;
  int ret = libncvarput1(ncid, varid, indexp, op);
  FUNCTION_EXIT;
  return ret;
}



int ncvarget1(int ncid, int varid, const long *indexp, void *ip) {
  FUNCTION_ENTRY;
  int ret = libncvarget1(ncid, varid, indexp, ip);
  FUNCTION_EXIT;
  return ret;
}



int ncvarput(int ncid, int varid, const long *startp, const long *countp, const void *op) {
  FUNCTION_ENTRY;
  int ret = libncvarput(ncid, varid, startp, countp, op);
  FUNCTION_EXIT;
  return ret;
}



int ncvarget(int ncid, int varid, const long *startp, const long *countp, void *ip) {
  FUNCTION_ENTRY;
  int ret = libncvarget(ncid, varid, startp, countp, ip);
  FUNCTION_EXIT;
  return ret;
}



int ncvarputs(int ncid, int varid, const long *startp, const long *countp, const long *stridep, const void *op) {
  FUNCTION_ENTRY;
  int ret = libncvarputs(ncid, varid, startp, countp, stridep, op);
  FUNCTION_EXIT;
  return ret;
}



int ncvargets(int ncid, int varid, const long *startp, const long *countp, const long *stridep, void *ip) {
  FUNCTION_ENTRY;
  int ret = libncvargets(ncid, varid, startp, countp, stridep, ip);
  FUNCTION_EXIT;
  return ret;
}



int ncvarputg(int ncid, int varid, const long *startp, const long *countp, const long *stridep, const long *imapp, const void *op) {
  FUNCTION_ENTRY;
  int ret = libncvarputg(ncid, varid, startp, countp, stridep, imapp, op);
  FUNCTION_EXIT;
  return ret;
}



int ncvargetg(int ncid, int varid, const long *startp, const long *countp, const long *stridep, const long *imapp, void *ip) {
  FUNCTION_ENTRY;
  int ret = libncvargetg(ncid, varid, startp, countp, stridep, imapp, ip);
  FUNCTION_EXIT;
  return ret;
}



int ncvarrename(int ncid, int varid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncvarrename(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}



int ncrecinq(int ncid, int *nrecvarsp, int *recvaridsp, long *recsizesp) {
  FUNCTION_ENTRY;
  int ret = libncrecinq(ncid, nrecvarsp, recvaridsp, recsizesp);
  FUNCTION_EXIT;
  return ret;
}



int ncrecget(int ncid, long recnum, void **datap) {
  FUNCTION_ENTRY;
  int ret = libncrecget(ncid, recnum, datap);
  FUNCTION_EXIT;
  return ret;
}



int ncrecput(int ncid, long recnum, void *const *datap) {
  FUNCTION_ENTRY;
  int ret = libncrecput(ncid, recnum, datap);
  FUNCTION_EXIT;
  return ret;
}



int nc_initialize(void) {
  FUNCTION_ENTRY;
  int ret = libnc_initialize();
  FUNCTION_EXIT;
  return ret;
}



int nc_finalize(void) {
  FUNCTION_ENTRY;
  int ret = libnc_finalize();
  FUNCTION_EXIT;
  return ret;
}



int nc_rc_set(const char *key, const char *value) {
  FUNCTION_ENTRY;
  int ret = libnc_rc_set(key, value);
  FUNCTION_EXIT;
  return ret;
}



PPTRACE_START_INTERCEPT_FUNCTIONS(netcdf)
   INTERCEPT3("nc_def_user_format", libnc_def_user_format)
 INTERCEPT3("nc_inq_user_format", libnc_inq_user_format)
 INTERCEPT3("nc_set_alignment", libnc_set_alignment)
 INTERCEPT3("nc_get_alignment", libnc_get_alignment)
 INTERCEPT3("nc__create", libnc__create)
 INTERCEPT3("nc_create", libnc_create)
 INTERCEPT3("nc__open", libnc__open)
 INTERCEPT3("nc_open", libnc_open)
 INTERCEPT3("nc_inq_path", libnc_inq_path)
 INTERCEPT3("nc_inq_ncid", libnc_inq_ncid)
 INTERCEPT3("nc_inq_grps", libnc_inq_grps)
 INTERCEPT3("nc_inq_grpname", libnc_inq_grpname)
 INTERCEPT3("nc_inq_grpname_full", libnc_inq_grpname_full)
 INTERCEPT3("nc_inq_grpname_len", libnc_inq_grpname_len)
 INTERCEPT3("nc_inq_grp_parent", libnc_inq_grp_parent)
 INTERCEPT3("nc_inq_grp_ncid", libnc_inq_grp_ncid)
 INTERCEPT3("nc_inq_grp_full_ncid", libnc_inq_grp_full_ncid)
 INTERCEPT3("nc_inq_varids", libnc_inq_varids)
 INTERCEPT3("nc_inq_dimids", libnc_inq_dimids)
 INTERCEPT3("nc_inq_typeids", libnc_inq_typeids)
 INTERCEPT3("nc_inq_type_equal", libnc_inq_type_equal)
 INTERCEPT3("nc_def_grp", libnc_def_grp)
 INTERCEPT3("nc_rename_grp", libnc_rename_grp)
 INTERCEPT3("nc_def_compound", libnc_def_compound)
 INTERCEPT3("nc_insert_compound", libnc_insert_compound)
 INTERCEPT3("nc_insert_array_compound", libnc_insert_array_compound)
 INTERCEPT3("nc_inq_type", libnc_inq_type)
 INTERCEPT3("nc_inq_typeid", libnc_inq_typeid)
 INTERCEPT3("nc_inq_compound", libnc_inq_compound)
 INTERCEPT3("nc_inq_compound_name", libnc_inq_compound_name)
 INTERCEPT3("nc_inq_compound_size", libnc_inq_compound_size)
 INTERCEPT3("nc_inq_compound_nfields", libnc_inq_compound_nfields)
 INTERCEPT3("nc_inq_compound_field", libnc_inq_compound_field)
 INTERCEPT3("nc_inq_compound_fieldname", libnc_inq_compound_fieldname)
 INTERCEPT3("nc_inq_compound_fieldindex", libnc_inq_compound_fieldindex)
 INTERCEPT3("nc_inq_compound_fieldoffset", libnc_inq_compound_fieldoffset)
 INTERCEPT3("nc_inq_compound_fieldtype", libnc_inq_compound_fieldtype)
 INTERCEPT3("nc_inq_compound_fieldndims", libnc_inq_compound_fieldndims)
 INTERCEPT3("nc_inq_compound_fielddim_sizes", libnc_inq_compound_fielddim_sizes)
 INTERCEPT3("nc_def_vlen", libnc_def_vlen)
 INTERCEPT3("nc_inq_vlen", libnc_inq_vlen)
 INTERCEPT3("nc_free_vlen", libnc_free_vlen)
 INTERCEPT3("nc_free_vlens", libnc_free_vlens)
 INTERCEPT3("nc_put_vlen_element", libnc_put_vlen_element)
 INTERCEPT3("nc_get_vlen_element", libnc_get_vlen_element)
 INTERCEPT3("nc_free_string", libnc_free_string)
 INTERCEPT3("nc_inq_user_type", libnc_inq_user_type)
 INTERCEPT3("nc_put_att", libnc_put_att)
 INTERCEPT3("nc_get_att", libnc_get_att)
 INTERCEPT3("nc_def_enum", libnc_def_enum)
 INTERCEPT3("nc_insert_enum", libnc_insert_enum)
 INTERCEPT3("nc_inq_enum", libnc_inq_enum)
 INTERCEPT3("nc_inq_enum_member", libnc_inq_enum_member)
 INTERCEPT3("nc_inq_enum_ident", libnc_inq_enum_ident)
 INTERCEPT3("nc_def_opaque", libnc_def_opaque)
 INTERCEPT3("nc_inq_opaque", libnc_inq_opaque)
 INTERCEPT3("nc_put_var", libnc_put_var)
 INTERCEPT3("nc_get_var", libnc_get_var)
 INTERCEPT3("nc_put_var1", libnc_put_var1)
 INTERCEPT3("nc_get_var1", libnc_get_var1)
 INTERCEPT3("nc_put_vara", libnc_put_vara)
 INTERCEPT3("nc_get_vara", libnc_get_vara)
 INTERCEPT3("nc_put_vars", libnc_put_vars)
 INTERCEPT3("nc_get_vars", libnc_get_vars)
 INTERCEPT3("nc_put_varm", libnc_put_varm)
 INTERCEPT3("nc_get_varm", libnc_get_varm)
 INTERCEPT3("nc_def_var_quantize", libnc_def_var_quantize)
 INTERCEPT3("nc_inq_var_quantize", libnc_inq_var_quantize)
 INTERCEPT3("nc_def_var_deflate", libnc_def_var_deflate)
 INTERCEPT3("nc_inq_var_deflate", libnc_inq_var_deflate)
 INTERCEPT3("nc_def_var_szip", libnc_def_var_szip)
 INTERCEPT3("nc_inq_var_szip", libnc_inq_var_szip)
 INTERCEPT3("nc_def_var_fletcher32", libnc_def_var_fletcher32)
 INTERCEPT3("nc_inq_var_fletcher32", libnc_inq_var_fletcher32)
 INTERCEPT3("nc_def_var_chunking", libnc_def_var_chunking)
 INTERCEPT3("nc_inq_var_chunking", libnc_inq_var_chunking)
 INTERCEPT3("nc_def_var_fill", libnc_def_var_fill)
 INTERCEPT3("nc_inq_var_fill", libnc_inq_var_fill)
 INTERCEPT3("nc_def_var_endian", libnc_def_var_endian)
 INTERCEPT3("nc_inq_var_endian", libnc_inq_var_endian)
 INTERCEPT3("nc_def_var_filter", libnc_def_var_filter)
 INTERCEPT3("nc_inq_var_filter", libnc_inq_var_filter)
 INTERCEPT3("nc_set_fill", libnc_set_fill)
 INTERCEPT3("nc_set_default_format", libnc_set_default_format)
 INTERCEPT3("nc_set_chunk_cache", libnc_set_chunk_cache)
 INTERCEPT3("nc_get_chunk_cache", libnc_get_chunk_cache)
 INTERCEPT3("nc_set_var_chunk_cache", libnc_set_var_chunk_cache)
 INTERCEPT3("nc_get_var_chunk_cache", libnc_get_var_chunk_cache)
 INTERCEPT3("nc_redef", libnc_redef)
 INTERCEPT3("nc__enddef", libnc__enddef)
 INTERCEPT3("nc_enddef", libnc_enddef)
 INTERCEPT3("nc_sync", libnc_sync)
 INTERCEPT3("nc_abort", libnc_abort)
 INTERCEPT3("nc_close", libnc_close)
 INTERCEPT3("nc_inq", libnc_inq)
 INTERCEPT3("nc_inq_ndims", libnc_inq_ndims)
 INTERCEPT3("nc_inq_nvars", libnc_inq_nvars)
 INTERCEPT3("nc_inq_natts", libnc_inq_natts)
 INTERCEPT3("nc_inq_unlimdim", libnc_inq_unlimdim)
 INTERCEPT3("nc_inq_unlimdims", libnc_inq_unlimdims)
 INTERCEPT3("nc_inq_format", libnc_inq_format)
 INTERCEPT3("nc_inq_format_extended", libnc_inq_format_extended)
 INTERCEPT3("nc_def_dim", libnc_def_dim)
 INTERCEPT3("nc_inq_dimid", libnc_inq_dimid)
 INTERCEPT3("nc_inq_dim", libnc_inq_dim)
 INTERCEPT3("nc_inq_dimname", libnc_inq_dimname)
 INTERCEPT3("nc_inq_dimlen", libnc_inq_dimlen)
 INTERCEPT3("nc_rename_dim", libnc_rename_dim)
 INTERCEPT3("nc_inq_att", libnc_inq_att)
 INTERCEPT3("nc_inq_attid", libnc_inq_attid)
 INTERCEPT3("nc_inq_atttype", libnc_inq_atttype)
 INTERCEPT3("nc_inq_attlen", libnc_inq_attlen)
 INTERCEPT3("nc_inq_attname", libnc_inq_attname)
 INTERCEPT3("nc_copy_att", libnc_copy_att)
 INTERCEPT3("nc_rename_att", libnc_rename_att)
 INTERCEPT3("nc_del_att", libnc_del_att)
 INTERCEPT3("nc_put_att_text", libnc_put_att_text)
 INTERCEPT3("nc_get_att_text", libnc_get_att_text)
 INTERCEPT3("nc_put_att_string", libnc_put_att_string)
 INTERCEPT3("nc_get_att_string", libnc_get_att_string)
 INTERCEPT3("nc_put_att_uchar", libnc_put_att_uchar)
 INTERCEPT3("nc_get_att_uchar", libnc_get_att_uchar)
 INTERCEPT3("nc_put_att_schar", libnc_put_att_schar)
 INTERCEPT3("nc_get_att_schar", libnc_get_att_schar)
 INTERCEPT3("nc_put_att_short", libnc_put_att_short)
 INTERCEPT3("nc_get_att_short", libnc_get_att_short)
 INTERCEPT3("nc_put_att_int", libnc_put_att_int)
 INTERCEPT3("nc_get_att_int", libnc_get_att_int)
 INTERCEPT3("nc_put_att_long", libnc_put_att_long)
 INTERCEPT3("nc_get_att_long", libnc_get_att_long)
 INTERCEPT3("nc_put_att_float", libnc_put_att_float)
 INTERCEPT3("nc_get_att_float", libnc_get_att_float)
 INTERCEPT3("nc_put_att_double", libnc_put_att_double)
 INTERCEPT3("nc_get_att_double", libnc_get_att_double)
 INTERCEPT3("nc_put_att_ushort", libnc_put_att_ushort)
 INTERCEPT3("nc_get_att_ushort", libnc_get_att_ushort)
 INTERCEPT3("nc_put_att_uint", libnc_put_att_uint)
 INTERCEPT3("nc_get_att_uint", libnc_get_att_uint)
 INTERCEPT3("nc_put_att_longlong", libnc_put_att_longlong)
 INTERCEPT3("nc_get_att_longlong", libnc_get_att_longlong)
 INTERCEPT3("nc_put_att_ulonglong", libnc_put_att_ulonglong)
 INTERCEPT3("nc_get_att_ulonglong", libnc_get_att_ulonglong)
 INTERCEPT3("nc_def_var", libnc_def_var)
 INTERCEPT3("nc_inq_var", libnc_inq_var)
 INTERCEPT3("nc_inq_varid", libnc_inq_varid)
 INTERCEPT3("nc_inq_varname", libnc_inq_varname)
 INTERCEPT3("nc_inq_vartype", libnc_inq_vartype)
 INTERCEPT3("nc_inq_varndims", libnc_inq_varndims)
 INTERCEPT3("nc_inq_vardimid", libnc_inq_vardimid)
 INTERCEPT3("nc_inq_varnatts", libnc_inq_varnatts)
 INTERCEPT3("nc_rename_var", libnc_rename_var)
 INTERCEPT3("nc_copy_var", libnc_copy_var)
 INTERCEPT3("nc_put_var1_text", libnc_put_var1_text)
 INTERCEPT3("nc_get_var1_text", libnc_get_var1_text)
 INTERCEPT3("nc_put_var1_uchar", libnc_put_var1_uchar)
 INTERCEPT3("nc_get_var1_uchar", libnc_get_var1_uchar)
 INTERCEPT3("nc_put_var1_schar", libnc_put_var1_schar)
 INTERCEPT3("nc_get_var1_schar", libnc_get_var1_schar)
 INTERCEPT3("nc_put_var1_short", libnc_put_var1_short)
 INTERCEPT3("nc_get_var1_short", libnc_get_var1_short)
 INTERCEPT3("nc_put_var1_int", libnc_put_var1_int)
 INTERCEPT3("nc_get_var1_int", libnc_get_var1_int)
 INTERCEPT3("nc_put_var1_long", libnc_put_var1_long)
 INTERCEPT3("nc_get_var1_long", libnc_get_var1_long)
 INTERCEPT3("nc_put_var1_float", libnc_put_var1_float)
 INTERCEPT3("nc_get_var1_float", libnc_get_var1_float)
 INTERCEPT3("nc_put_var1_double", libnc_put_var1_double)
 INTERCEPT3("nc_get_var1_double", libnc_get_var1_double)
 INTERCEPT3("nc_put_var1_ushort", libnc_put_var1_ushort)
 INTERCEPT3("nc_get_var1_ushort", libnc_get_var1_ushort)
 INTERCEPT3("nc_put_var1_uint", libnc_put_var1_uint)
 INTERCEPT3("nc_get_var1_uint", libnc_get_var1_uint)
 INTERCEPT3("nc_put_var1_longlong", libnc_put_var1_longlong)
 INTERCEPT3("nc_get_var1_longlong", libnc_get_var1_longlong)
 INTERCEPT3("nc_put_var1_ulonglong", libnc_put_var1_ulonglong)
 INTERCEPT3("nc_get_var1_ulonglong", libnc_get_var1_ulonglong)
 INTERCEPT3("nc_put_var1_string", libnc_put_var1_string)
 INTERCEPT3("nc_get_var1_string", libnc_get_var1_string)
 INTERCEPT3("nc_put_vara_text", libnc_put_vara_text)
 INTERCEPT3("nc_get_vara_text", libnc_get_vara_text)
 INTERCEPT3("nc_put_vara_uchar", libnc_put_vara_uchar)
 INTERCEPT3("nc_get_vara_uchar", libnc_get_vara_uchar)
 INTERCEPT3("nc_put_vara_schar", libnc_put_vara_schar)
 INTERCEPT3("nc_get_vara_schar", libnc_get_vara_schar)
 INTERCEPT3("nc_put_vara_short", libnc_put_vara_short)
 INTERCEPT3("nc_get_vara_short", libnc_get_vara_short)
 INTERCEPT3("nc_put_vara_int", libnc_put_vara_int)
 INTERCEPT3("nc_get_vara_int", libnc_get_vara_int)
 INTERCEPT3("nc_put_vara_long", libnc_put_vara_long)
 INTERCEPT3("nc_get_vara_long", libnc_get_vara_long)
 INTERCEPT3("nc_put_vara_float", libnc_put_vara_float)
 INTERCEPT3("nc_get_vara_float", libnc_get_vara_float)
 INTERCEPT3("nc_put_vara_double", libnc_put_vara_double)
 INTERCEPT3("nc_get_vara_double", libnc_get_vara_double)
 INTERCEPT3("nc_put_vara_ushort", libnc_put_vara_ushort)
 INTERCEPT3("nc_get_vara_ushort", libnc_get_vara_ushort)
 INTERCEPT3("nc_put_vara_uint", libnc_put_vara_uint)
 INTERCEPT3("nc_get_vara_uint", libnc_get_vara_uint)
 INTERCEPT3("nc_put_vara_longlong", libnc_put_vara_longlong)
 INTERCEPT3("nc_get_vara_longlong", libnc_get_vara_longlong)
 INTERCEPT3("nc_put_vara_ulonglong", libnc_put_vara_ulonglong)
 INTERCEPT3("nc_get_vara_ulonglong", libnc_get_vara_ulonglong)
 INTERCEPT3("nc_put_vara_string", libnc_put_vara_string)
 INTERCEPT3("nc_get_vara_string", libnc_get_vara_string)
 INTERCEPT3("nc_put_vars_text", libnc_put_vars_text)
 INTERCEPT3("nc_get_vars_text", libnc_get_vars_text)
 INTERCEPT3("nc_put_vars_uchar", libnc_put_vars_uchar)
 INTERCEPT3("nc_get_vars_uchar", libnc_get_vars_uchar)
 INTERCEPT3("nc_put_vars_schar", libnc_put_vars_schar)
 INTERCEPT3("nc_get_vars_schar", libnc_get_vars_schar)
 INTERCEPT3("nc_put_vars_short", libnc_put_vars_short)
 INTERCEPT3("nc_get_vars_short", libnc_get_vars_short)
 INTERCEPT3("nc_put_vars_int", libnc_put_vars_int)
 INTERCEPT3("nc_get_vars_int", libnc_get_vars_int)
 INTERCEPT3("nc_put_vars_long", libnc_put_vars_long)
 INTERCEPT3("nc_get_vars_long", libnc_get_vars_long)
 INTERCEPT3("nc_put_vars_float", libnc_put_vars_float)
 INTERCEPT3("nc_get_vars_float", libnc_get_vars_float)
 INTERCEPT3("nc_put_vars_double", libnc_put_vars_double)
 INTERCEPT3("nc_get_vars_double", libnc_get_vars_double)
 INTERCEPT3("nc_put_vars_ushort", libnc_put_vars_ushort)
 INTERCEPT3("nc_get_vars_ushort", libnc_get_vars_ushort)
 INTERCEPT3("nc_put_vars_uint", libnc_put_vars_uint)
 INTERCEPT3("nc_get_vars_uint", libnc_get_vars_uint)
 INTERCEPT3("nc_put_vars_longlong", libnc_put_vars_longlong)
 INTERCEPT3("nc_get_vars_longlong", libnc_get_vars_longlong)
 INTERCEPT3("nc_put_vars_ulonglong", libnc_put_vars_ulonglong)
 INTERCEPT3("nc_get_vars_ulonglong", libnc_get_vars_ulonglong)
 INTERCEPT3("nc_put_vars_string", libnc_put_vars_string)
 INTERCEPT3("nc_get_vars_string", libnc_get_vars_string)
 INTERCEPT3("nc_put_varm_text", libnc_put_varm_text)
 INTERCEPT3("nc_get_varm_text", libnc_get_varm_text)
 INTERCEPT3("nc_put_varm_uchar", libnc_put_varm_uchar)
 INTERCEPT3("nc_get_varm_uchar", libnc_get_varm_uchar)
 INTERCEPT3("nc_put_varm_schar", libnc_put_varm_schar)
 INTERCEPT3("nc_get_varm_schar", libnc_get_varm_schar)
 INTERCEPT3("nc_put_varm_short", libnc_put_varm_short)
 INTERCEPT3("nc_get_varm_short", libnc_get_varm_short)
 INTERCEPT3("nc_put_varm_int", libnc_put_varm_int)
 INTERCEPT3("nc_get_varm_int", libnc_get_varm_int)
 INTERCEPT3("nc_put_varm_long", libnc_put_varm_long)
 INTERCEPT3("nc_get_varm_long", libnc_get_varm_long)
 INTERCEPT3("nc_put_varm_float", libnc_put_varm_float)
 INTERCEPT3("nc_get_varm_float", libnc_get_varm_float)
 INTERCEPT3("nc_put_varm_double", libnc_put_varm_double)
 INTERCEPT3("nc_get_varm_double", libnc_get_varm_double)
 INTERCEPT3("nc_put_varm_ushort", libnc_put_varm_ushort)
 INTERCEPT3("nc_get_varm_ushort", libnc_get_varm_ushort)
 INTERCEPT3("nc_put_varm_uint", libnc_put_varm_uint)
 INTERCEPT3("nc_get_varm_uint", libnc_get_varm_uint)
 INTERCEPT3("nc_put_varm_longlong", libnc_put_varm_longlong)
 INTERCEPT3("nc_get_varm_longlong", libnc_get_varm_longlong)
 INTERCEPT3("nc_put_varm_ulonglong", libnc_put_varm_ulonglong)
 INTERCEPT3("nc_get_varm_ulonglong", libnc_get_varm_ulonglong)
 INTERCEPT3("nc_put_varm_string", libnc_put_varm_string)
 INTERCEPT3("nc_get_varm_string", libnc_get_varm_string)
 INTERCEPT3("nc_put_var_text", libnc_put_var_text)
 INTERCEPT3("nc_get_var_text", libnc_get_var_text)
 INTERCEPT3("nc_put_var_uchar", libnc_put_var_uchar)
 INTERCEPT3("nc_get_var_uchar", libnc_get_var_uchar)
 INTERCEPT3("nc_put_var_schar", libnc_put_var_schar)
 INTERCEPT3("nc_get_var_schar", libnc_get_var_schar)
 INTERCEPT3("nc_put_var_short", libnc_put_var_short)
 INTERCEPT3("nc_get_var_short", libnc_get_var_short)
 INTERCEPT3("nc_put_var_int", libnc_put_var_int)
 INTERCEPT3("nc_get_var_int", libnc_get_var_int)
 INTERCEPT3("nc_put_var_long", libnc_put_var_long)
 INTERCEPT3("nc_get_var_long", libnc_get_var_long)
 INTERCEPT3("nc_put_var_float", libnc_put_var_float)
 INTERCEPT3("nc_get_var_float", libnc_get_var_float)
 INTERCEPT3("nc_put_var_double", libnc_put_var_double)
 INTERCEPT3("nc_get_var_double", libnc_get_var_double)
 INTERCEPT3("nc_put_var_ushort", libnc_put_var_ushort)
 INTERCEPT3("nc_get_var_ushort", libnc_get_var_ushort)
 INTERCEPT3("nc_put_var_uint", libnc_put_var_uint)
 INTERCEPT3("nc_get_var_uint", libnc_get_var_uint)
 INTERCEPT3("nc_put_var_longlong", libnc_put_var_longlong)
 INTERCEPT3("nc_get_var_longlong", libnc_get_var_longlong)
 INTERCEPT3("nc_put_var_ulonglong", libnc_put_var_ulonglong)
 INTERCEPT3("nc_get_var_ulonglong", libnc_get_var_ulonglong)
 INTERCEPT3("nc_put_var_string", libnc_put_var_string)
 INTERCEPT3("nc_get_var_string", libnc_get_var_string)
 INTERCEPT3("nc_reclaim_data", libnc_reclaim_data)
 INTERCEPT3("nc_reclaim_data_all", libnc_reclaim_data_all)
 INTERCEPT3("nc_copy_data", libnc_copy_data)
 INTERCEPT3("nc_copy_data_all", libnc_copy_data_all)
 INTERCEPT3("nc_dump_data", libnc_dump_data)
 INTERCEPT3("nc_put_att_ubyte", libnc_put_att_ubyte)
 INTERCEPT3("nc_get_att_ubyte", libnc_get_att_ubyte)
 INTERCEPT3("nc_put_var1_ubyte", libnc_put_var1_ubyte)
 INTERCEPT3("nc_get_var1_ubyte", libnc_get_var1_ubyte)
 INTERCEPT3("nc_put_vara_ubyte", libnc_put_vara_ubyte)
 INTERCEPT3("nc_get_vara_ubyte", libnc_get_vara_ubyte)
 INTERCEPT3("nc_put_vars_ubyte", libnc_put_vars_ubyte)
 INTERCEPT3("nc_get_vars_ubyte", libnc_get_vars_ubyte)
 INTERCEPT3("nc_put_varm_ubyte", libnc_put_varm_ubyte)
 INTERCEPT3("nc_get_varm_ubyte", libnc_get_varm_ubyte)
 INTERCEPT3("nc_put_var_ubyte", libnc_put_var_ubyte)
 INTERCEPT3("nc_get_var_ubyte", libnc_get_var_ubyte)
 INTERCEPT3("nc_set_log_level", libnc_set_log_level)
 INTERCEPT3("nc_show_metadata", libnc_show_metadata)
 INTERCEPT3("nc_delete", libnc_delete)
 INTERCEPT3("nc__create_mp", libnc__create_mp)
 INTERCEPT3("nc__open_mp", libnc__open_mp)
 INTERCEPT3("nc_delete_mp", libnc_delete_mp)
 INTERCEPT3("nc_set_base_pe", libnc_set_base_pe)
 INTERCEPT3("nc_inq_base_pe", libnc_inq_base_pe)
 INTERCEPT3("nctypelen", libnctypelen)
 INTERCEPT3("nccreate", libnccreate)
 INTERCEPT3("ncopen", libncopen)
 INTERCEPT3("ncsetfill", libncsetfill)
 INTERCEPT3("ncredef", libncredef)
 INTERCEPT3("ncendef", libncendef)
 INTERCEPT3("ncsync", libncsync)
 INTERCEPT3("ncabort", libncabort)
 INTERCEPT3("ncclose", libncclose)
 INTERCEPT3("ncinquire", libncinquire)
 INTERCEPT3("ncdimdef", libncdimdef)
 INTERCEPT3("ncdimid", libncdimid)
 INTERCEPT3("ncdiminq", libncdiminq)
 INTERCEPT3("ncdimrename", libncdimrename)
 INTERCEPT3("ncattput", libncattput)
 INTERCEPT3("ncattinq", libncattinq)
 INTERCEPT3("ncattget", libncattget)
 INTERCEPT3("ncattcopy", libncattcopy)
 INTERCEPT3("ncattname", libncattname)
 INTERCEPT3("ncattrename", libncattrename)
 INTERCEPT3("ncattdel", libncattdel)
 INTERCEPT3("ncvardef", libncvardef)
 INTERCEPT3("ncvarid", libncvarid)
 INTERCEPT3("ncvarinq", libncvarinq)
 INTERCEPT3("ncvarput1", libncvarput1)
 INTERCEPT3("ncvarget1", libncvarget1)
 INTERCEPT3("ncvarput", libncvarput)
 INTERCEPT3("ncvarget", libncvarget)
 INTERCEPT3("ncvarputs", libncvarputs)
 INTERCEPT3("ncvargets", libncvargets)
 INTERCEPT3("ncvarputg", libncvarputg)
 INTERCEPT3("ncvargetg", libncvargetg)
 INTERCEPT3("ncvarrename", libncvarrename)
 INTERCEPT3("ncrecinq", libncrecinq)
 INTERCEPT3("ncrecget", libncrecget)
 INTERCEPT3("ncrecput", libncrecput)
 INTERCEPT3("nc_initialize", libnc_initialize)
 INTERCEPT3("nc_finalize", libnc_finalize)
 INTERCEPT3("nc_rc_set", libnc_rc_set)

PPTRACE_END_INTERCEPT_FUNCTIONS(netcdf)

static void init_netcdf() {
  INSTRUMENT_FUNCTIONS(netcdf);

  if (eztrace_autostart_enabled())
    eztrace_start();

  _netcdf_initialized = 1;
}

static void finalize_netcdf() {
  _netcdf_initialized = 0;

  eztrace_stop();
}

static void _netcdf_init(void) __attribute__((constructor));
static void _netcdf_init(void) {
  eztrace_log(dbg_lvl_debug, "eztrace_netcdf constructor starts\n");
  EZT_REGISTER_MODULE(netcdf, "Module for the netcdf library",
		      init_netcdf, finalize_netcdf);
  eztrace_log(dbg_lvl_debug, "eztrace_netcdf constructor ends\n");
}
