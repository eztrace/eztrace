/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */
#ifndef POSIX_IO_OTF2_H
#define POSIX_IO_OTF2_H

#include <stdio.h>
#include <eztrace-lib/eztrace.h>

struct ezt_file_handle {
  FILE* stream;
  int fd;
  int io_handle_ref;
  char *filename;
  struct ezt_file_handle*next;
};

extern struct ezt_file_handle* open_files;


struct context{
  struct ezt_file_handle *handle;
  int matching_id;
};


void init_otf2_posixio();

void otf2_fopen_file(const char* pathname, const char*mode, FILE*stream);
void otf2_fclose_file(FILE* stream);

void otf2_dup_fd(int oldfd, int new_fd);

void otf2_open_file(const char*pathname, int flags, int fd);
void otf2_close_file(int fd);

void otf2_begin_fd_operation(int fd,
			     OTF2_IoOperationMode mode,
			     size_t count,
			     struct context*context);
void otf2_begin_stream_operation(FILE* stream,
				 OTF2_IoOperationMode mode,
				 size_t count,
				 struct context*context);

void otf2_end_fd_operation(struct context* context, size_t nbytes) ;
void otf2_end_stream_operation(struct context* context, size_t nbytes);

void otf2_fd_seek_operation(int fd, off_t offset, int whence, off_t result);
void otf2_stream_seek_operation(FILE* stream, off_t offset, int whence, off_t result);

#endif	/* POSIX_IO_OTF2_H */
