/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include "mpi_eztrace.h"

#include <dlfcn.h>
#include <eztrace-lib/eztrace.h>
#include <mpi.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <unistd.h>
#include <eztrace-core/eztrace_attributes.h>

static void MPI_Testall_prolog(int count MAYBE_UNUSED,
			       MPI_Fint* reqs MAYBE_UNUSED,
			       int* flag MAYBE_UNUSED,
                               MPI_Status* s  MAYBE_UNUSED,
                               size_t size MAYBE_UNUSED,
			       int* isvalid) {
  for(int i=0; i<count; i++) {
    /* Make sure that requests passed to mpi_complete_request are valid.
     * This prevents from completing the same request several times
     */
    isvalid[i] = GET_ARRAY_ITEM(reqs, size, i) != MPI_REQUEST_NULL;
  }
}

static int MPI_Testall_core(int count,
			    MPI_Request* reqs,
			    int* flag,
                            MPI_Status* s) {
  return libMPI_Testall(count, reqs, flag, s);
}

static void MPI_Testall_epilog(int count MAYBE_UNUSED,
			       MPI_Fint* reqs MAYBE_UNUSED,
			       int* flag MAYBE_UNUSED,
                               MPI_Status* s  MAYBE_UNUSED,
                               size_t size MAYBE_UNUSED,
			       int* isvalid) {
  if(*flag) {
    for(int i=0; i<count; i++) {
      if(isvalid[i])
	mpi_complete_request(GET_ARRAY_ITEM(reqs, size, i), &s[i]);
    }
  }
}

int MPI_Testall(int count,
		MPI_Request* reqs,
		int* flag,
		MPI_Status* s) {
  FUNCTION_ENTRY;
  ALLOCATE_ITEMS(int, count, isvalid_static, isvalid);

  MPI_Status ezt_mpi_status[count];
  if(s == MPI_STATUSES_IGNORE)
    s = ezt_mpi_status;

  MPI_Testall_prolog(count, (MPI_Fint*)reqs, flag, s, sizeof(MPI_Request), isvalid);
  int ret = MPI_Testall_core(count, reqs, flag, s);
  MPI_Testall_epilog(count, (MPI_Fint*)reqs, flag, s, sizeof(MPI_Request), isvalid);

  FUNCTION_EXIT;
  return ret;
}

void mpif_testall_(int* count,
		   MPI_Fint* r,
		   int* index,
		   MPI_Status* s,
                  int* error) {
  FUNCTION_ENTRY_("mpi_testall_");
  int i;
  ALLOCATE_ITEMS(MPI_Request, *count, c_req, p_req);
  ALLOCATE_ITEMS(int, *count, isvalid_static, isvalid);

  for (i = 0; i < *count; i++)
    p_req[i] = MPI_Request_f2c(r[i]);

  MPI_Testall_prolog(*count, r, index, s, sizeof(MPI_Fint), isvalid);

  *error = MPI_Testall_core(*count, p_req, index, s);
  for (i = 0; i < *count; i++)
    r[i] = MPI_Request_c2f(p_req[i]);

  MPI_Testall_epilog(*count, r, index, s, sizeof(MPI_Fint), isvalid);
  FUNCTION_EXIT_("mpi_testall_");
}
