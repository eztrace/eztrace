/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include "mpi_eztrace.h"

#include <dlfcn.h>
#include <eztrace-lib/eztrace.h>
#include <mpi.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <unistd.h>
#include <eztrace-core/eztrace_attributes.h>

static void MPI_Waitall_prolog(int count MAYBE_UNUSED,
			       MPI_Fint* req MAYBE_UNUSED,
                               MPI_Status* s  MAYBE_UNUSED,
                               size_t size MAYBE_UNUSED,
			       int* isvalid MAYBE_UNUSED) {
}

static int MPI_Waitall_core(int count,
			    MPI_Request* req,
			    MPI_Status* s) {
  return libMPI_Waitall(count, req, s);
}



static void MPI_Waitall_epilog(int count MAYBE_UNUSED,
			       MPI_Fint* req MAYBE_UNUSED,
                               MPI_Status* s  MAYBE_UNUSED,
                               size_t size MAYBE_UNUSED,
			       int* isvalid) {
  for(int i=0; i<count; i++){
    if(isvalid[i]) {
      mpi_complete_request(GET_ARRAY_ITEM(req, size, i), &s[i]); 
    }
  }
}

int MPI_Waitall(int count, MPI_Request* req, MPI_Status* s) {
  FUNCTION_ENTRY;

  ALLOCATE_ITEMS(int, count, isvalid_static, isvalid);

  for(int i=0; i<count; i++) {
    /* Make sure that requests passed to mpi_complete_request are valid.
     * This prevents from completing the same request several times
     */
    isvalid[i] = req[i] != MPI_REQUEST_NULL;
  }

  MPI_Status ezt_mpi_status[count];
  if(s == MPI_STATUSES_IGNORE)
    s = ezt_mpi_status;
  
  MPI_Waitall_prolog(count, (MPI_Fint*) req, s, sizeof(MPI_Request), isvalid);
  int ret = MPI_Waitall_core(count, req, s);

  MPI_Waitall_epilog(count, (MPI_Fint*) req, s, sizeof(MPI_Request), isvalid);
  FUNCTION_EXIT;
  return ret;
}

void mpif_waitall_(int* c, MPI_Fint* r, MPI_Status* s, int* error) {
  FUNCTION_ENTRY_("mpi_waitall_");
  int i;
  ALLOCATE_ITEMS(int, *c, isvalid_static, isvalid);

  MPI_Waitall_prolog(*c, r, s, sizeof(MPI_Fint), isvalid);

  /* allocate a MPI_Request array and convert all the fortran requests
   * into C requests
   */
  ALLOCATE_ITEMS(MPI_Request, *c, c_req, p_req);
  for (i = 0; i < *c; i++) {
    p_req[i] = MPI_Request_f2c(r[i]);
    isvalid[i] = p_req[i] != MPI_REQUEST_NULL;
  }

  /* call the C version of MPI_Wait */
  *error = MPI_Waitall_core(*c, p_req, s);

  /* Since the requests may have been modified by MPI_Waitall,
   * we need to convert them back to Fortran
   */
  for (i = 0; i < *c; i++)
    r[i] = MPI_Request_c2f(p_req[i]);

  MPI_Waitall_epilog(*c, r, s, sizeof(MPI_Fint), isvalid);

  FUNCTION_EXIT_("mpi_waitall_");
}
