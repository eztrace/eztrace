#ifndef EZTRACE_INTERNALS_H
#define EZTRACE_INTERNALS_H

enum todo_status {
		  status_invalid,
		  not_initialized,
		  init_started,
		  init_stopped,
		  init_complete,
};

//struct ezt_internal_module {
//  char name[128];
//  enum ezt_internal_module_status status;
//  void (*init_function)(struct ezt_internal_module*);
//  void *module_data;
//  /* todo: add dependencies ? */
//};
//
#define STR_STATUS(status) \
  (status)==not_initialized ? "Not initialized" :\
    (status)==init_started ? "Initialization started" :	\
    (status)==init_stopped ? "Initialization stopped" :	\
    (status)==init_complete ? "Initialization complete" :	\
    "Invalid"

#define NB_DEPEND_MAX 30
#define STRING_MAXLEN 128
struct todo_dependency {
  char dep_name[STRING_MAXLEN];
  enum todo_status status;
};

struct ezt_internal_todo {
  char name[STRING_MAXLEN];
  void (*todo_function)();
  struct todo_dependency dependencies[NB_DEPEND_MAX];
  int nb_dependencies;
};

void todo_print_list();
void enqueue_todo(const char* name, void (*function)(), const char* dependency,
		  enum todo_status dep_status);

void add_todo_dependency(const char* name, const char* dependency,
			 enum todo_status dep_status);

enum todo_status todo_get_status(const char* todo_name);

void todo_set_status(const char* todo_name,
		     enum todo_status status);

void todo_progress();

/* wait until module_name initialization is complete */
void todo_wait(const char* todo_name, enum todo_status status);

#endif  /* EZTRACE_INTERNALS */
