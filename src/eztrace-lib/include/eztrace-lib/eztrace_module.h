#ifndef EZTRACE_MODULE_H
#define EZTRACE_MODULE_H

#include <eztrace-core/eztrace_list.h>


/* Describe a plugin */
struct eztrace_module {
  void (*init)();
  void (*finalize)();
  char name[128];
  char description[128];
  struct ezt_instrumented_function *functions;
  struct ezt_list_token_t token;
};

#define EZT_STR(s) #s
#define EZT_REGISTER_MODULE(_mod_name, _mod_descr, _mod_init_function, _mod_finalize_function) do { \
    static struct eztrace_module module;				\
    sprintf(module.name, EZT_STR(_mod_name) );				\
    sprintf(module.description, EZT_STR(_mod_descr) );			\
    module.functions = PPTRACE_SYMBOL_LIST(_mod_name);			\
    module.init = _mod_init_function;					\
    module.finalize = _mod_finalize_function;				\
    eztrace_register_module(&module);					\
  } while(0)

void eztrace_register_module(struct eztrace_module* p_module);

/* load all the modules specified by the EZTRACE_TRACE variable */
void eztrace_load_modules(int mod_verb);

/* load all the modules specified by the EZTRACE_TRACE variable */
void eztrace_load_all_modules(int mod_verb);

void eztrace_print_module_list();

int initialize_modules();
void finalize_modules();
#endif	/* EZTRACE_MODULE_H */
