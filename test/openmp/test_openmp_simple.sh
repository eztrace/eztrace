#!/bin/bash
CUR_PATH=$(dirname $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="openmp_simple"
export OMP_NUM_THREADS=4

run_and_check_command "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "openmp" "./$name" || ((nb_fail++))

trace_filename="${name}_trace/eztrace_log.otf2"
trace_check_integrity "$trace_filename" || exit 1
trace_check_enter_leave_parity  "$trace_filename"

nb_locks=100
nb_calls=$(echo "$nb_locks * 2"|bc)

trace_check_event_type "$trace_filename" "THREAD_TEAM_BEGIN" 48
trace_check_event_type "$trace_filename" "THREAD_TEAM_END" 48

trace_check_event_type "$trace_filename" "THREAD_FORK" 12
trace_check_event_type "$trace_filename" "THREAD_JOIN" 12

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
