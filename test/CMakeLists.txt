enable_testing()

set(EZTRACE_LIBRARY_PATH "${CMAKE_BINARY_DIR}/src/eztrace-lib")
set(TEST_ENVIRONMENT  "EZTRACE_AVAIL_PATH=${CMAKE_BINARY_DIR}/src/eztrace_avail"
  "EZTRACE_CC_PATH=${CMAKE_BINARY_DIR}/src/modules/omp/bin/eztrace_cc"
  "LIBRARY_PATH=$ENV{LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/omp"
  "LD_LIBRARY_PATH=$ENV{LD_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/omp"
  "EZTRACE_PATH=${CMAKE_BINARY_DIR}/src/eztrace"
  "OTF2_PRINT_PATH=${OTF2_PRINT}"
  "EZTRACE_TEST_OPTION=-p"
)


add_subdirectory (unit_tests)

if (EZTRACE_ENABLE_COMPILER_INSTRUMENTATION)
  add_subdirectory (compiler_instrumentation)
endif()

if (EZTRACE_ENABLE_MPI)
  add_subdirectory (mpi)
endif()

if (EZTRACE_ENABLE_MEMORY)
  add_subdirectory (memory)
endif()

if (EZTRACE_ENABLE_OPENMP)
  add_subdirectory (openmp)
endif()

if (EZTRACE_ENABLE_OMPT)
  add_subdirectory (ompt)
endif()

if (EZTRACE_ENABLE_PTHREAD)
  add_subdirectory (pthread)
endif()

if (EZTRACE_ENABLE_POSIXIO)
  add_subdirectory (posixio)
endif()

