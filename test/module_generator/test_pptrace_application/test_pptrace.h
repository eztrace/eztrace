/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef __TEST_PPTRACE_H__
#define __TEST_PPTRACE_H__

#include <stdint.h>

double test_pptrace_foo(uint32_t*, uint32_t);

#endif	/* __TEST_PPTRACE_H__ */
