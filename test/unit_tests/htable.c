#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <eztrace_htable.h>


#if 0
void test1() {
  struct ezt_hashtable ht;
  ezt_hashtable_init(&ht, sizeof(int), 64, NULL);

  int key_int=42;
  char* key=(char*)&key_int;
  uint64_t value = 17;

  /* get non-existing key in an empty hashtable*/
  void * ret = ezt_hashtable_get(&ht, key);
  assert( ret == NULL);

  /* insert a key,value, then retrieve it */
  ezt_hashtable_insert(&ht, key, &value);
  ret = ezt_hashtable_get(&ht, key);
  assert( ret == &value);

  /* get non-existing key in a non-empty hashtable*/
  int key2_int = key_int+1;
  char*key2 = (char*)&key2_int;
  ret = ezt_hashtable_get(&ht, key2);
  assert( ret == NULL);

  /* get non-existing key in an hashtable that used to contain the key*/
  ezt_hashtable_remove(&ht, key);
  ret = ezt_hashtable_get(&ht, key);
  assert( ret == NULL);

  /* remove a non existing key */
  ezt_hashtable_remove(&ht, key2);

  ezt_hashtable_finalize(&ht);
  printf("%s OK\n", __func__);
}

#endif

int main(int argc __attribute__((unused)),
	 char** argv __attribute__((unused))) {

  //  test1();
  
  return EXIT_SUCCESS;
}
