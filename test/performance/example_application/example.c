/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "example.h"

int lib_function(double *t, int size, int n) {
  int res = n;
  int i;
  for(i=0; i<NITER_COMPUTE; i++) {
    res +=(int) t[(i*n)%size];
  }
  return res;
}

int lib_instrumented_function(double *t, int size, int n) {
  int res = n;
  int i;
  for(i=0; i<NITER_COMPUTE; i++) {
    res +=(int) t[(i*n)%size];
  }
  return res;
}
