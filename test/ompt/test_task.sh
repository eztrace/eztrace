#!/bin/bash
CUR_PATH=$(dirname $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="task"

nb_tasks=$("$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "ompt" "./test_$name" 2>&1 | grep "Number of executed tasks" | cut -d' ' -f5)

trace_filename="test_${name}_trace/eztrace_log.otf2"

trace_check_integrity "$trace_filename"
#trace_check_enter_leave_parity  "$trace_filename"


trace_check_event_type "$trace_filename" "THREAD_TASK_CREATE" $nb_tasks
trace_check_event_type "$trace_filename" "THREAD_TASK_SWITCH" "$((2*nb_tasks))"


trace_check_nb_enter "$trace_filename" "OpenMP task" "$((2*nb_tasks))"
trace_check_nb_leave "$trace_filename" "OpenMP task" "$((2*nb_tasks))"

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
