#!/bin/bash

eztrace_version=2.1.0
docker build -f Dockerfile_mpich . -t eztrace/eztrace.mpich:$eztrace_version
docker build -f Dockerfile_mpich_vite . -t eztrace/eztrace.mpich.vite:$eztrace_version
docker build -f Dockerfile_openmpi . -t eztrace/eztrace.openmpi:$eztrace_version
docker build -f Dockerfile_openmpi_vite . -t eztrace/eztrace.openmpi.vite:$eztrace_version


docker build -f Dockerfile_test_mpich . -t eztrace/eztrace-test:eztrace.mpich
docker build -f Dockerfile_test_openmpi . -t eztrace/eztrace-test:eztrace.openmpi


docker tag eztrace/eztrace.mpich:$eztrace_version eztrace/eztrace.mpich:latest
docker tag eztrace/eztrace.mpich.vite:$eztrace_version eztrace/eztrace.mpich.vite:latest
docker tag eztrace/eztrace.openmpi:$eztrace_version  eztrace/eztrace.openmpi:latest
docker tag eztrace/eztrace.openmpi.vite:$eztrace_version  eztrace/eztrace.openmpi.vite:latest
