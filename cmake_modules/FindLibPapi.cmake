# - Find libpapi include dirs and libraries
# Use this module by invoking find_package with the form:
# find_package(LIBPAPI [REQUIRED]) # Fail with error if libpapi is not found
# This module finds libpapi headers and library.
# Results are reported in variables:
# LIBPAPI_FOUND - True if headers and requested libraries were found
# LIBPAPI_INCLUDE_DIRS - libpapi include directories
# LIBPAPI_LIBRARIES - libpapi component libraries to be linked
# -------------------------------------------------------------------------------------

if (LIBPAPI_LIBRARIES AND LIBPAPI_INCLUDE_DIRS)
  set (LibPapi_FIND_QUIETLY TRUE)
endif (LIBPAPI_LIBRARIES AND LIBPAPI_INCLUDE_DIRS)

unset(_inc_env)
string(REPLACE ":" ";" _path_env "$ENV{include}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{C_include_PATH}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{CPATH}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{include_PATH}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{PATH}")
list(APPEND _inc_env "${_path_env}")
list(REMOVE_DUPLICATES _inc_env)

find_path (LIBPAPI_INCLUDE_DIRS
NAMES
papi.h
PATHS
${_inc_env}
/usr/include
/usr/include/libpapi
/usr/local/include
/usr/local/include/libpapi
/opt/local/include
/opt/local/include/libpapi
ENV CPATH)

## Search library in user defined path (via LD_LIBRARY_PATH and LIBRARY_PATH)
unset(_lib_env)
string(REPLACE ":" ";" _lib_env "$ENV{LD_LIBRARY_PATH}")
list(APPEND _lib_env "${_path_env}")
string(REPLACE ":" ";" _lib_env "$ENV{LIBRARY_PATH}")
list(APPEND _lib_env "${_path_env}")
string(REPLACE ":" ";" _lib_env "$ENV{MIC_LD_LIBRARY_PATH}")
list(APPEND _lib_env "${_path_env}")
list(REMOVE_DUPLICATES _lib_env)

find_library (
  LIBPAPI_LIBRARIES
  NAMES papi
  HINTS ${_lib_env}
  NO_DEFAULT_PATH
)

## Search in standard path
if (NOT LIBPAPI_LIBRARIES)
  unset(_lib_env)
  list(APPEND _lib_env "/usr/local/lib64")
  list(APPEND _lib_env "/usr/lib64")
  list(APPEND _lib_env "/usr/local/lib")
  list(APPEND _lib_env "/usr/lib")

  find_library (
    LIBPAPI_LIBRARIES
    NAMES papi
    PATHS ${_lib_env}
    )
endif()

include (FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set LIBPAPI_FOUND to TRUE if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LibPapi DEFAULT_MSG
LIBPAPI_LIBRARIES
LIBPAPI_INCLUDE_DIRS
)

mark_as_advanced(LIBPAPI_INCLUDE_DIRS LIBPAPI_LIBRARIES)