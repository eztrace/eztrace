#
# Find the OMPT header
#
# - Find OMPT include dirs and libraries
# Use this module by invoking find_package with the form:
#  find_package(OMPT
#               [REQUIRED]) # Fail with error if hwloc is not found
#
# OMPT_FOUND        - True if headers and requested libraries were found
# OMP_INCLUDE_PATH   - OMPT include directories
# OMP_LIBRARY_PATH   - Link directory for OMPT
# OMP_LIBRARY       - The libraries needed to use OMPT
# HAVE_OMPT_51      - True if OMPT version is greater or equal to 5.1
#
# OMPT_PATH can be used to make it simpler to find the various include
# directories and compiled libraries when OMPT was not installed in the
# usual/well-known directories (e.g. because you made an in tree-source
# compilation or because you installed it in an "unusual" directory).
# Just set OMPT_PATH it to your specific installation directory


try_compile(COMPILES_OMPT "${CMAKE_BINARY_DIR}/temp" "${CMAKE_SOURCE_DIR}/test/ompt//test_ompt.c"
  LINK_OPTIONS -fopenmp
  CMAKE_FLAGS
  "-fopenmp")

check_c_source_compiles("
#include <ompt.h>
int main() {
  ompt_sync_region_t kind = ompt_sync_region_barrier_implicit_workshare;
  return 0;
}
"
  HAVE_OMPT_51)

if(HAVE_OMPT_51)
  SET(CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} -DHAVE_OMPT_51=1")
endif()

if(COMPILES_OMPT)
  # the compiler is able to compile an ompt program (eg. we're using clang)
  set(OMPT_FOUND TRUE)
  mark_as_advanced(OMPT_INCLUDE_PATH OMPT_LIBRARY_PATH OMPT_LIBRARY)
else()
  # the compiler can't compile an ompt program (eg. we're using gcc)
  # let's see if ompt.h and libomp are available


  find_path(OMPT_INCLUDE_PATH ompt.h
    HINTS ${OMPT_INCLUDE_DIRS} ${OMPT_PATH}/include)

  find_library(OMPT_LIBRARY NAMES omp iomp5 gomp 
    HINTS ${OMPT_LIBDIR} ${OMPT_LIBRARY_PATH} ${OMPT_PATH}/lib
    ${OMPT_PATH}/lib/* NO_DEFAULT_PATH)

  IF(OMPT_LIBRARY)
    MESSAGE ( STATUS "Found OMPT library: ${OMPT_LIBRARY}" )
    GET_FILENAME_COMPONENT(OMPT_LIBRARY_tmp "${OMPT_LIBRARY}" PATH)
    SET (OMPT_LIBRARY_PATH ${OMPT_LIBRARY_tmp} CACHE PATH "")
  ELSE(OMPT_LIBRARY)
    SET (OMPT_LIBRARY_PATH "OMPT_LIBRARY_PATH-NOTFOUND")
    unset(LIBRARY_PATH CACHE)
  ENDIF(OMPT_LIBRARY)

  if((OMPT_LIBRARY) AND (OMPT_LIBRARY_PATH) AND (OMPT_INCLUDE_DIR))
    SET( OMPT_FOUND TRUE )
  else()
    MESSAGE(STATUS "OMPT installation was not found. Please provide OMPT_PATH:")
    MESSAGE(STATUS "  - through the GUI when working with ccmake, ")
    MESSAGE(STATUS "  - as a command line argument when working with cmake e.g.")
    MESSAGE(STATUS "    cmake .. -DOMPT_PATH:PATH=/usr/local/ompt ")
    SET(OMPT_PATH "" CACHE PATH "Root of OMPT install tree." )
    SET(OMPT_PATH "" CACHE PATH "Root of OMPT install tree." )
  endif()

  mark_as_advanced(OMPT_INCLUDE_PATH OMPT_LIBRARY_PATH OMPT_LIBRARY)
endif()
